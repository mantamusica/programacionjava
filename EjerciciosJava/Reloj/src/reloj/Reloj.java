package reloj;

public class Reloj {
    int hora,min,seg;

    public void setHora(int hora) {
        this.hora = hora;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setSeg(int seg) {
        this.seg = seg;
    }
    
    public int getHora() {
        return hora;
    }

    public int getMin() {
        return min;
    }

    public int getSeg() {
        return seg;
    }
    
    public void Incrementar() {
        seg = seg + 1;
        if (seg > 59){
            seg = 0;
            min = min + 1;
        }
        if (seg > 59){
        hora = hora + 1; // Lo deje aqui 
        }
    }
    
}
