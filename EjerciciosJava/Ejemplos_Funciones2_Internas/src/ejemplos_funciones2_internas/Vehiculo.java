package ejemplos_funciones2_internas;

public class Vehiculo {
    	public int ruedas; 
   	private double velocidad=0;
        //No se puede acceder a este atributo desde un archivo "Externo" por ser private

    	public void acelerar(double cantidad) { 
        		velocidad += cantidad; 
       	}
   	public void frenar(double cantidad) { 
        		velocidad -= cantidad; 
   	}
   	public double obtenerVelocidad(){ 
       		return velocidad; 
    	}
}