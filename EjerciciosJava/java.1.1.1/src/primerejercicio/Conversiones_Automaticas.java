/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package primerejercicio;

public class Conversiones_Automaticas {
       public static void main(String[] args) {
       int int1,int2;
       char chark;
       byte bytes=70;
       float floath=9.2f;
       long longc=1000000000;
       //conversiones explícitas
       int1=(int)longc;
       chark=(char)bytes; /*cuando la variable destino es char y el origen es 
         numérico, la conversión no puede ser automática*/
        int2=(int)floath;/*trunca la parte decimal*/
        System.out.println("El valor de longc(tipo long) es "+longc+ " y ese valor en un int ha quedado en "+int1);
        System.out.println("El valor de bytes(tipo byte) es "+bytes+ " y ese valor en un char ha quedado en "+chark);
        System.out.println("El valor de floath(tipo float) es "+floath+ " y ese valor en un int ha quedado en "+int2);
    }
}
