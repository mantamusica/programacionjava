package primerejercicio;

public class Mases {
    public static void main(String[] args){
        int T = 4;
        System.out.println("El Valor Que Contiene La Variable Es: " +T++); // Muestra el contenido y luego suma
        System.out.println("El Valor Que Contiene La Variable Es: " + ++T); // Suma 2 veces (Cada + es [+1])
        System.out.println("El Valor Que Contiene La Variable Es: " +T ++); // Suma despues Por Tanto Se Queda Igual
        // Sería Igual Con El --
    }
}