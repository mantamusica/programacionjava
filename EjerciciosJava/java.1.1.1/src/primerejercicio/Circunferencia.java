package primerejercicio;
import java.util.Scanner;

public class Circunferencia {

    public static void main(String[] args) {
        double radio;
        double Superficie;
        double Long;
        double Volumen;
        
        final double pi = 3.14;// final declarar constantes inamovibles
        Scanner teclado =  new Scanner(System.in);
        radio=teclado.nextInt();
        Long = 2 * pi * radio;
        System.out.printf("%,.2f",Long);
        System.out.println(" ");
        Superficie = pi * Math.pow(+radio, 2);
        System.out.printf("%,.2f",Superficie); //Math.pow(+numer, 2);
        System.out.println(" ");
        Volumen = (4 * pi * Math.pow(+radio, 3)/3);
        System.out.printf("%,.2f",Volumen);
        System.out.println(" ");
        
        //Long = 2 pi R
        //Superficie = pi R^2
        //Volumen = (4 pi R^3)/3
        
    }
    
}
