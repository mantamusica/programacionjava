package ejemplos_funciones3_This;

public class Punto { 
    int posX; 
    int posY; 
void modificarCoords(int posX, int posY){ 
/* Hay ambigüedad ya que posX es el nombre de uno de 
* los parámetros, y además el nombre de una de las 
* propiedades de la clase Punto 
*/ 
this.posX=posX; //this permite evitar la ambigüedad 
this.posY=posY; 
} 
}