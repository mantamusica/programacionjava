package hoja_3;

import java.util.Scanner;

public class Ejercicio_4 {

    public static void main(String[] args) {
        int num;
        System.out.print("Introduce Un Numero: ");
        Scanner teclado = new Scanner(System.in);
        num = teclado.nextInt();
        if (num >= 0)  {
                if (num % 2 == 0) {
                    System.out.println("El numero Es Par");
                } else { 
                   // for({valor inicial};{condición de termino};{factor de incremento del valor inicial}){
                   //acá va lo que se repetirá n veces de acuerdo a la condición de termino                                     
                        for (int i = num; i >= 0; i=i-2) {
                            System.out.println(i);
                            }
                }         
        } else {
            System.out.println("El Numero Es Menor De 0");
        }
    }
}