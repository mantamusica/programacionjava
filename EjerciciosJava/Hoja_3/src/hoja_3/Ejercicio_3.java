package hoja_3;

import java.util.Scanner;

public class Ejercicio_3 {

    public static void main(String[] args) {
        int num, num2;
        System.out.print("Introduce Un Primer Numero: ");
        Scanner teclado = new Scanner(System.in);
        num = teclado.nextInt();
        System.out.print("Introduce Un Segundo Numero: ");
        num2 = teclado.nextInt();
        if (num > num2) {
            System.out.println("El Numero " +num +" Es Mayor Que Numero " +num2); // meter variables con print(ln)con + y con printf con comas
        } else {
            System.out.println("El Numero "+num2 +" Es Mayor Que Numero " +num);
        }

    }
}
