package hoja_3;

import java.util.Scanner;

public class Ejercicio_1 {

    public static void main(String[] args) {
        int num;
        System.out.print("Introduce Un Numero: ");
        Scanner teclado = new Scanner(System.in);
        num = teclado.nextInt();
        if (num >= 1 & num <= 10) {
            if (num % 2 == 0) {
                System.out.println("El Numero Es Par");
            } else {
                System.out.println("El Numero Es Impar");
            }
        } else {
            System.out.println("El Numero introducido Se Pasa Del rango 1-10");
        }
    }

}
