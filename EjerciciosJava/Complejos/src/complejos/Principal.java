package complejos;
import java.util.*;

public class Principal {

    public static void main(String[] args) {
        double Comple1_real, Comple1_img;
        
        Scanner teclado = new Scanner(System.in);
        
        Complejo complejo1 = new Complejo();
        
        System.out.print("Introduce La Parte Real: ");
        Comple1_real = teclado.nextDouble();
        complejo1.setReal(Comple1_real);
        
        System.out.print("Introduce La Parte Imaginaria: ");
        Comple1_img = teclado.nextDouble();
        complejo1.setImg(Comple1_img);
        
        System.out.println("Numero Complejo1: " + complejo1.toString());
        
        Complejo complejo2 = new Complejo(2,3);
        System.out.print("Numero Complejo2: ");
        complejo2.print();

        complejo1.Sumar(complejo2);
        
        System.out.print("La Suma De " +Comple1_real+ " "+ Comple1_img+ "i y "+complejo2.toString() );
        System.out.print(" Es: ");
        complejo1.print();
        
    }
    
}
