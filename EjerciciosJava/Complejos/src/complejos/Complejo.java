package complejos;

public class Complejo {
    double img,real;
    char Sumando;

    public Complejo() {
        
    }
    
    public Complejo(double real, double img) {
        this.img = img;
        this.real = real;
    }

    public double getReal() {
        return real;
    }
    
    public double getImg() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }
    
    public void setImg(double img) {
        this.img = img;
    }
    
    @Override
    public String toString() {
        if (img < 0) {
            Sumando = ' ';
        }else{
            Sumando = '+';
        }
        return " " + real +" "+ Sumando + " " + img + "i";
    }
    
    public void print() {
        System.out.println(toString());
    }
    
    public void Sumar (Complejo b) {
        this.real=this.real+b.real;
        this.img=this.img+b.img;
    }
}
