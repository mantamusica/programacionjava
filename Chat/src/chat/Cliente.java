package chat;

import java.io.*;
import java.net.*;

public class Cliente {

    public static void main(String[] args) {
        
        Leer leer=new Leer();
        try {
            Socket canalComunicacion = null;
            
            OutputStream bufferSalida;
            DataOutputStream datos;
            
            canalComunicacion = new Socket("127.0.0.1", 10831);
            bufferSalida = canalComunicacion.getOutputStream();
            datos = new DataOutputStream(bufferSalida);
            String mensaje = leer.Cadena("Introduce una palabra para enviar");
            
            datos.writeUTF(mensaje);
            
            datos.writeUTF("");
            datos.close();
            bufferSalida.close();
            canalComunicacion.close();
        } catch (UnknownHostException ex) {
           System.out.println("Error");
        } catch (IOException ex) {
            System.out.println("Error");
        }

    }
}
