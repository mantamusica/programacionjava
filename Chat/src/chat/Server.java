/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    public static void main(String[] args) {
        boolean salir=false;
        try {
            ServerSocket puerto = null;
            Socket canalComunicacion = null;
            InputStream bufferEntrada;
            DataInputStream datos;
            puerto = new ServerSocket(10831);
            canalComunicacion = puerto.accept();
            bufferEntrada = canalComunicacion.getInputStream();
            datos = new DataInputStream(bufferEntrada);
            String cadena = new String(datos.readUTF());
            while(salir==false){
            while (cadena.length() > 0) {
                System.out.print(cadena);
                cadena = datos.readUTF();
            }
            }
            datos.close();
            bufferEntrada.close();
            canalComunicacion.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}