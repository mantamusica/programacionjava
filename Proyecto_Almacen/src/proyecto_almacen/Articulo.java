/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_almacen;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class Articulo implements Serializable {

    PedirDatos lector = new PedirDatos();

    public String Codigo;
    public String Descripcion;
    public int Existencias;

    Articulo() {

    }

    Articulo(String Codigo, String Descripcion, int Existencias) {
        this.Codigo = Codigo;
        this.Descripcion = Descripcion;
        this.Existencias = Existencias;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getExistencias() {
        return Existencias;
    }

    public void setExistencias(int Existencias) {
        this.Existencias = Existencias;
    }

    void Escribir() throws FileNotFoundException, IOException {

        String Codigo, Descripcion;
        int Existencias;

        FileOutputStream fos;
        ObjectOutputStream oos;

        fos = new FileOutputStream("C:/Users/Usuario/Desktop/Almacen.dat");
        oos = new ObjectOutputStream(fos);

        Codigo = lector.Leer_String("Codigo: ");

        Descripcion = lector.Leer_String("Descripcion: ");
        
        do {
            Existencias = lector.Numero_Int("Existencias: ");
            
            if ((Existencias < -1) | (Existencias > 1000)){
                System.out.println("Numero No Valido");
            }
            
        } while ((Existencias < -1) | (Existencias > 1000));
        

        Articulo Objeto = new Articulo(Codigo, Descripcion, Existencias);

        oos.writeObject(Objeto);

        oos.close();
        fos.close();

    }

    public void Leer_Fichero(String fichero) throws ClassNotFoundException, IOException {
        FileInputStream fs;
        ObjectInputStream os = null;
        boolean eof_fich = false;
        int cont = 1;

        try {
            fs = new FileInputStream(fichero);
            os = new ObjectInputStream(fs);

            while (eof_fich != true) {
                Articulo art;
                art = (Articulo) os.readObject();
                System.out.println(cont + "--> Codigo: " + art.Codigo + " Descripcion: " + art.Descripcion + " Existencias: " + art.Existencias);
                cont++;
            }
        } catch (EOFException eeof) {
            eof_fich = true;
        } catch (IOException ex) {
            System.out.println("Error leer");
        }
        
        os.close();
    }

    public void Que_Articulos_Pedir(String fichero) throws ClassNotFoundException, FileNotFoundException, IOException {

        boolean encontrado = false;

        FileInputStream fis = new FileInputStream(fichero);
        ObjectInputStream ois = new ObjectInputStream(fis);

        try {
            while (true) {
                Articulo art = (Articulo) ois.readObject();

                if (art.getExistencias() < 10) {
                    System.out.println(art.getCodigo());
                    encontrado = true;
                }
            }

        } catch (EOFException error) {

        }

        ois.close();
        fis.close();
        

        if (!encontrado) {
            System.out.println("No Hace Falta Pedir Ningun Articulo");
        }
    }

    public void BorrarFichero(String fichero) throws ClassNotFoundException, FileNotFoundException, IOException  {
        File fich = new File(fichero);
        
        boolean Exitoso = fich.delete();
        if (Exitoso) {
            System.out.println("Borrado Completado");
        } else {
            System.out.println("No se puede borrar el fichero " + fichero);
        }

    }

    @Override
    public String toString() {
        return "Articulo{" + "Existencias=" + Existencias + '}';
    }

}
