package proyecto_almacen;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        int opcion;
        String Fichero = "C:/Users/Usuario/Desktop/Almacen.dat";
        
        PedirDatos lector = new PedirDatos();
        Articulo objetos_almacenados = new Articulo();

        do {

            System.out.println("1. Cargar Articulo");
            System.out.println("2. Visualiza ");
            System.out.println("3. Pedidos Necesarios De Realizar (Aquellos cuyas existencias Sean menor de 10)");
            System.out.println("4. Cantidad De Articulos Distintos En El Almacén");
            System.out.println("5. Total De Articulos En El Almacén Contando El Numero De Existencias");
            System.out.println("6. Modificar El Numero De Existencias De Un Articulo");
            System.out.println("7. Borrar Fichero");
            System.out.println("8. Copia De Seguridad Del Almacén");
            System.out.println("9. Visualizar Copia De Seguridad De Almacén");
            System.out.println("10. Salir");

            opcion = lector.Numero_Int("Introduce La Opcion Elegida: ");

            switch (opcion) {

                case 1:
                    objetos_almacenados.Escribir();
                    break;

                case 2:
                    objetos_almacenados.Leer_Fichero(Fichero);
                    break;

                case 3:
                    objetos_almacenados.Que_Articulos_Pedir(Fichero);
                    break;

                case 4:
                    break;

                case 5:
                    break;

                case 6:
                    break;

                case 7:
                    objetos_almacenados.BorrarFichero(Fichero);
                    break;

                case 8:
                    break;

                case 9:
                    break;

                case 10:
                    opcion=10;
                    break;

            }

        } while (opcion != 10);
    }

}
