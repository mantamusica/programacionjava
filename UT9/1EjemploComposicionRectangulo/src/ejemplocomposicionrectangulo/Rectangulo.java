
package ejemplocomposicionrectangulo;

public class Rectangulo {
     private Punto vertice1;
     private Punto vertice2;
     
     public Rectangulo ()
    {
        this.vertice1= new Punto (0,0);
        this.vertice2= new Punto (1,1);
    }

     
    public Rectangulo (double x1, double y1, double x2, double y2)
    {   
        this.vertice1= new Punto (x1, y1);
        this.vertice2= new Punto (x2, y2);
    }

    public Rectangulo(Punto vertice1, Punto vertice2) {
        this.vertice1 = vertice1;
        this.vertice2 = vertice2;
    }
     
    public Rectangulo (double base,double altura){
        this.vertice1=new Punto(0,0);
        this.vertice2=new Punto(base,altura);
    }
    
    
    // Constructor copia
    public Rectangulo (Rectangulo r) {
    this.vertice1= r.getVertice1();
    this.vertice2= r.getVertice2();
}



    public Punto getVertice1() {
        return vertice1;
    }

    public void setVertice1(Punto vertice1) {
        this.vertice1 = vertice1;
    }

    public Punto getVertice2() {
        return vertice2;
    }

    public void setVertice2(Punto vertice2) {
        this.vertice2 = vertice2;
    }
    
    
    public double calcularSuperficie () {
        double area, base, altura;   // Variables locales
    
        base= vertice2.getX () - vertice1.getX ();  //Cuando los atributos de la clase rectangulo eran 4 doubles era x2 - x1
        altura= vertice2.getY () - vertice1.getY (); // Antes era y2 - y1
        area= base * altura;
        return area;
    }

    public double CalcularPerimetro () {
        double perimetro, base, altura;   // Variables locales
    
         base= vertice2.getX () - vertice1.getX ();  
        altura= vertice2.getY () - vertice1.getY (); 
        perimetro= 2*base + 2*altura;
        return perimetro;
    }

    
}
