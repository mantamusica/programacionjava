
package rectanguloSinComposicion;

public class Rectangulo {
    private double x1,y1,x2,y2;

    public Rectangulo() {
    }

    public Rectangulo(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    
     public double calcularSuperficie () {
        double area, base, altura;   // Variables locales
        base= x2-x1;  
        altura= y2-y1; 
        area= base * altura;
        return area;
    }

    public double CalcularPerimetro () {
        double perimetro, base, altura;   // Variables locales
        base= x2-x1;  
        altura= y2-y1; 
        perimetro= 2*base + 2*altura;
        return perimetro;
    }
}
