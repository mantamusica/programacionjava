/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package C_clasesAbstractas;

/**
 *
 * @author SYSTEM
 */
public abstract class Instrumento {

    public abstract void tocar();

    public abstract void tocar(String nota);

    public abstract void afinar();
}
