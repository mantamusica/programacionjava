/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package C_clasesAbstractas;

/**
 *
 * @author SYSTEM
 */
public class Percusion extends Instrumento {

    public Percusion() {
    }

    public void afinar() {
        System.out.println("Percusion.afinar()");
    }

    public void tocar() {
        System.out.println("Percusion.tocar()");
    }

    public void tocar(String nota) {
        System.out.println("Percusion.tocar()" + nota);
    }
}
