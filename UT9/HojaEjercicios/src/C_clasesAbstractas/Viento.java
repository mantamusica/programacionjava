/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package C_clasesAbstractas;

/**
 *
 * @author SYSTEM
 */
public class Viento extends Instrumento {

    public Viento() {
    }

    public void afinar() {
        System.out.println("Viento.afinar()");
    }

    public void tocar() {
        System.out.println("Viento.tocar()");
    }

    public void tocar(String nota) {
        System.out.println("Viento.tocar()" + nota);
    }
}
