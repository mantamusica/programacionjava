/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package C_clasesAbstractas;

import java.util.Random;

/**
 *
 * @author SYSTEM
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String[] notas = {"Do", "Re", "Mi", "Fa", "Sol", "La", "Si"};
        Instrumento orquesta[] = new Instrumento[10]; //Creación de array polimórfico de instrumentos.
//Ejemplo sencillo en el que se crea variable polimórfica de tipo instrumento y se le asigna un instrumento de viento.
        Instrumento unInstrumento; // Se crea una variable de tipo intrumento (no se puede instanciar esta clase al ser abstracta).
        unInstrumento = new Viento();
        unInstrumento.tocar();
//---------------------------------------------
        //Se rellena el array de instrumentos con instrumentos de todo tipo.
        int opcion;
        for (int i = 0; i < 10; i++) {
            opcion = new Random().nextInt(3) + 1;
            switch (opcion) {
                case 1:
                    orquesta[i] = new Viento();break;
                case 2:
                    orquesta[i] = new Percusion();break;
                case 3:
                    orquesta[i] = new Cuerda(); break;
            }
        }
        for (int i = 0; i < 10; i++) {
            orquesta[i].afinar();
        }
        for (int i = 0; i < 10; i++) {
            orquesta[i].tocar();
        }
        for (int i = 0; i < 10; i++) {
            orquesta[i].tocar(notas[new Random().nextInt(6)]);
        }
    }

}
