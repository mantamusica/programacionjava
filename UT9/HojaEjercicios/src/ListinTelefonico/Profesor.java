/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ListinTelefonico;

/**
 *
 * @author SYSTEM
 */
public class Profesor extends Persona {
    String idProfesor;

    public Profesor() {
    }

    public Profesor( String nombre, String apellidos, int edad,String idProfesor) {
        super(nombre, apellidos, edad);
        this.idProfesor = idProfesor;
    }

    public String getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(String idProfesor) {
        this.idProfesor = idProfesor;
    }
    
    public void mostrarDatos(){
        System.out.println("Nombre: "+getNombre());
        System.out.println("Apellidos: "+getApellidos());
        System.out.println("Edad: "+getEdad());
        System.out.println("Datos profesor");
        System.out.println("ID del profesor: "+idProfesor);
    } 
}
