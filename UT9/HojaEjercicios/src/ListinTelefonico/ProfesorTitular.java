/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ListinTelefonico;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ProfesorTitular extends Profesor {

    private Calendar fechaComienzo;

    public ProfesorTitular() {
    }

    public ProfesorTitular(String nombre, String apellidos,
            int edad, String idProfesor, Calendar fechaComienzo) {
        super(nombre, apellidos, edad, idProfesor);
        this.fechaComienzo = fechaComienzo;
    }

    public void mostrarDatos() {
        super.mostrarDatos();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String Stringfecha = formatoFecha.format(this.fechaComienzo.getTime());

        System.out.println("Datos profeso titular");
        System.out.println("Fecha comienzo: " + Stringfecha);
    }
}
