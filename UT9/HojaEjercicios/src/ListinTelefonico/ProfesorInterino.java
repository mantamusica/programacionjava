/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ListinTelefonico;

/**
 *
 * @author SYSTEM
 */
public class ProfesorInterino extends Profesor {
   int experiencia; 

    public ProfesorInterino() {
    }

    public ProfesorInterino( String nombre, String apellidos, int edad, String idProfesor,int experiencia) {
        super(nombre, apellidos, edad, idProfesor);
        this.experiencia = experiencia;
    }
    
    public void mostrarDatos(){
       super.mostrarDatos();
       System.out.println("Datos profesor interino");
        System.out.println("Experiencia: "+experiencia);
   }
   
}
