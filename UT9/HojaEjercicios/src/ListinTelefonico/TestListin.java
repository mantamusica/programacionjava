/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ListinTelefonico;

import java.util.Calendar;

/**
 *
 * @author SYSTEM
 */
public class TestListin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Profesor profesor1 = new Profesor ("Juan", "Hernández García", 33,"Prof 22-387-11");

 
        Calendar fecha1 = null ;
        fecha1.set(2019,10,22); //Los meses van de 0 a 11, luego 10 representa noviembre
        ProfesorTitular titular1=new ProfesorTitular("Alvaro", "Gutierrez Jorrin",54
                ,"ProfTit 11-123-32",fecha1);
        
        ProfesorInterino interino1 = new ProfesorInterino("José Luis", "Morales Pérez", 44,"ProfInt 12-380-31", 10);
 
        ListinProfesores listin1 = new ListinProfesores ();
        listin1.addProfesor(profesor1);
        listin1.addProfesor(titular1);
        listin1.addProfesor(interino1);
        
        listin1.listar(); } //Cierre del main

    }

