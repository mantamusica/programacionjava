/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package B_ligDinamica;

/**
 *
 * @author SYSTEM
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void muevete(Mamifero m) {
        m.mover();
    }

    public static void main(String[] args) {
        // TODO code application logic here

        Mamifero animal1, animal2;
        
        animal1 = new Perro();
        muevete(animal1);
        
        animal2 = new Gato();
        muevete(animal2);

    }
}
