/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package D_interfaces;

/**
 *
 * @author SYSTEM
 */
public class Elemento implements Nombrable{

    String nombre;

    public Elemento(String nom) {
        nombre = nom;
    }

// obligatorio implementar método mostrarNombre
    public void mostrarNombre() {
        System.out.println("Nombre: " + nombre);
        if (CIERTO) {
            System.out.println("Constante CIERTO ");
        }
    }
}
