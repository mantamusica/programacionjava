/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package A_sobrecarga;

/**
 *
 * @author SYSTEM
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Articulo articulo1=new Articulo();
        
        articulo1.setPrecio();
        System.out.println("Llamando al método setPrecio() "+articulo1.getPrecio());
        
        articulo1.setPrecio((float)7.50);
        System.out.println("Llamando al método setPrecio(float nuevoPrecio)"
                +articulo1.getPrecio());
        
        articulo1.setPrecio(1, 20);
        System.out.println("Llamando al metodo setPrcio(float costo, int porcentajeGanancia)"
                +articulo1.getPrecio());
    }
}
