/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package A_sobrecarga;

public class Articulo {

    private float precio;

    public float getPrecio() {
        return precio;
    }

    public void setPrecio() {
        precio = (float) 3.50;
    }

    public void setPrecio(float nuevoPrecio) {
        precio = nuevoPrecio;
    }

    public void setPrecio(float costo, int porcentajeGanancia) {
        precio = costo + (costo * porcentajeGanancia/100);
    }
}
