/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploclaseherenciapersona;

import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public class Principal {
    public static void main(String[] args) {
        
        Persona persona = new Persona("Pablo","Perez Ruiz",new GregorianCalendar (1990,7,12));
        persona.mostrar();
        
        Alumno alumno=new Alumno("Pedro","Lopez",new GregorianCalendar (1990,7,12),"1-b",7);
        alumno.mostrar();
        
        Profesor profesor =new Profesor("Ana","Ruiz",new GregorianCalendar (1990,7,12),"Ciencias",12000);
        profesor.mostrar();
        
        
        
    }
}
