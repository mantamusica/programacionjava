/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploclaseherenciapersona;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */

public class Persona {
        String nombre;
        String apellidos;
        GregorianCalendar fechaNacim;
      

     String getNombre() {
       
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public GregorianCalendar getFechaNacim() {
        return fechaNacim;
    }

    public void setFechaNacim(GregorianCalendar fechaNacim) {
        this.fechaNacim = fechaNacim;
    }
         
     public void  mostrar(){
         SimpleDateFormat formatoFecha =new SimpleDateFormat ("dd/MM/yyyy");
         String Stringfecha= formatoFecha.format(this.fechaNacim.getTime());
         
         System.out.printf("Nombre: %s\n",this.getNombre());
         System.out.printf("Apellidos: %s\n",this.apellidos);
         System.out.printf("Fecha de Nacimiento: %s\n",Stringfecha);
         
     }
public Persona (String nombre, String apellidos, GregorianCalendar fechaNacim) {           
            this.nombre= nombre;
            this.apellidos= apellidos;
            this.fechaNacim= (GregorianCalendar) fechaNacim.clone();
        }

}
