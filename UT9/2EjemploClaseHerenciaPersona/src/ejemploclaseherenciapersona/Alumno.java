/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploclaseherenciapersona;

import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public class Alumno  extends Persona{
     String grupo;
     double notaMedia; 
     
    


    public String getGrupo() {
        return grupo;
    }

    public double getNotaMedia() {
        return notaMedia;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setNotaMedia(double notaMedia) {
        this.notaMedia = notaMedia;
    }
  
    
    
    public Alumno (String nombre, String apellidos, GregorianCalendar fechaNacim, String grupo, double notaMedia) {
     super (nombre, apellidos, fechaNacim);
     this.grupo= grupo;
     this.notaMedia= notaMedia;
}
    
    
    
    @Override
        public String getNombre() {//sobrescribo el nombre del profesor
        String cadena="ALUMNO:"+nombre;
        return cadena;
    }
        
        public void mostrar(){
            super.mostrar();//llamamos al mostrar de la clase padre, mediante super.
            System.out.println("Grupo:"+grupo);
            System.out.println("Nota Media:"+notaMedia);
        }

}
