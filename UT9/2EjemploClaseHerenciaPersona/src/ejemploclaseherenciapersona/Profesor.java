/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploclaseherenciapersona;

import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public class Profesor extends Persona {

    String especialidad;
    double salario;

    public String getEspecialidad() {
        return especialidad;
    }

    public double getSalario() {
        return salario;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getNombre() {//sobrescribo el nombre del profesor
        String cadena = "PROFESOR:" + nombre;
        return cadena;
    }

    public Profesor(String nombre, String apellidos, GregorianCalendar fechaNacim, String especialidad, double salario) {
        super(nombre, apellidos, fechaNacim);
        this.especialidad = especialidad;
        this.salario = salario;
    }

    public void mostrar() {
        super.mostrar();//llamamos al mostrar de la clase padre, mediante super.
        System.out.println("Especialidad:" + especialidad);
        System.out.println("Salario:" + salario);
    }
}
