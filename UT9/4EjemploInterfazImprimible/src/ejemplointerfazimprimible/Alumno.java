/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplointerfazimprimible;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public class Alumno extends Persona{
    
    protected String grupo;
    protected double notaMedia;

    
    public Alumno (String nombre, String apellidos, GregorianCalendar fechaNacim, String grupo, double notaMedia) {
        super (nombre, apellidos, fechaNacim);
        this.grupo= grupo;
        this.notaMedia= notaMedia;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setNotaMedia(double notaMedia) {
        this.notaMedia = notaMedia;
    }

    public String getGrupo() {
        return grupo;
    }

    public double getNotaMedia() {
        return notaMedia;
    }
    
    @Override
    public void mostrar(){
        SimpleDateFormat formatoFecha=new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Nombre: "+nombre);
        System.out.println("Apellidos: "+apellidos);
        System.out.println("F.Nacimiento: "+formatoFecha.format(fechaNacim.getTime()));
        System.out.println("Grupo: "+grupo);
        System.out.println("Nota media: "+notaMedia);
    }
     
    @Override
    public String getNombre() {
        return "ALUMNO: "+this.nombre;
    }

    @Override
    public String devolverContenidoString(){
        String cadena=super.devolverContenidoString()+"\nGrupo: "+grupo+"\nNota media: "+notaMedia;
        return cadena;
    }
    
    @Override
    public ArrayList devolverContenidoArrayList(){
        ArrayList ar;
        ar = super.devolverContenidoArrayList();
        ar.add(grupo);
        ar.add(notaMedia);
        return ar;
    }
    
}

