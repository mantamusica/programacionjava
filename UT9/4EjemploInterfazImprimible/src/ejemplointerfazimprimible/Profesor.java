/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplointerfazimprimible;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public class Profesor extends Persona {
    
    protected String especialidad;
    protected double salario;
    
    public Profesor (String nombre, String apellidos, GregorianCalendar fechaNacim, String especialidad, double salario) {
        super (nombre, apellidos, fechaNacim);
        this.especialidad= especialidad;
        this.salario= salario;
    }


    public String getEspecialidad() {
        return especialidad;
    }

    public double getSalario() {
        return salario;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
     
    @Override
    public void mostrar(){
        SimpleDateFormat formatoFecha=new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Nombre: "+nombre);
        System.out.println("Apellidos: "+apellidos);
        System.out.println("F.Nacimiento: "+formatoFecha.format(fechaNacim.getTime()));
        System.out.println("Especialidad: "+especialidad);
        System.out.printf("Salario: %5.2f\n",salario);
    }
     
    @Override
    public String getNombre() {
        return "PROFESOR: "+this.getNombre();
    }
    
    @Override
    public String devolverContenidoString(){
        String cadena=super.devolverContenidoString()+"\nEspecialidad: "+especialidad+"\nSalario: "+salario;
        return cadena;
    }
    
    @Override
    public ArrayList devolverContenidoArrayList(){
        ArrayList ar;
        ar = super.devolverContenidoArrayList();
        ar.add(especialidad);
        ar.add(salario);
        return ar;
    }
    
}

