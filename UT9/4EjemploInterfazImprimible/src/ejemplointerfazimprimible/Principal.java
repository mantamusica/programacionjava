/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplointerfazimprimible;

import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Profesor prof1=new Profesor("Benjamin","Perez",new GregorianCalendar(1980,10,16),"Informatica",2129);
        Alumno alum1=new Alumno("Alexander","Vicente",new GregorianCalendar(1994,10,16),"DAM1",2);

        System.out.println(alum1.devolverContenidoString());
        System.out.println(alum1.devolverContenidoArrayList());
        System.out.println();
        System.out.println(prof1.devolverContenidoString());
        System.out.println(prof1.devolverContenidoArrayList());        
        
    }
}
