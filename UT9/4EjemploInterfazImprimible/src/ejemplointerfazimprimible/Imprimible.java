/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplointerfazimprimible;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public interface Imprimible {
    
    public String devolverContenidoString();
    
    public ArrayList devolverContenidoArrayList();
    
}
