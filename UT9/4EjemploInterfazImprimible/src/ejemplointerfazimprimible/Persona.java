/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplointerfazimprimible;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author usuario
 */
public abstract class Persona implements Imprimible {
    
    protected String nombre;
    protected String apellidos;
    protected GregorianCalendar fechaNacim;
     
    public Persona (String nombre, String apellidos, GregorianCalendar fechaNacim) {
        this.nombre= nombre;
        this.apellidos= apellidos;
        this.fechaNacim= fechaNacim;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setFechaNacim(GregorianCalendar fechaNacim) {
        this.fechaNacim = fechaNacim;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public GregorianCalendar getFechaNacim() {
        return fechaNacim;
    }

    public abstract void mostrar();
    
    @Override
    public String devolverContenidoString(){
        SimpleDateFormat formatoFecha=new SimpleDateFormat("dd/MM/yyyy");
        String cadena="Nombre: "+nombre+"\nApellidos: "+apellidos+"\nF.Nacimiento: "+
                formatoFecha.format(fechaNacim.getTime());
        return cadena;
    }
    
    @Override
    public ArrayList devolverContenidoArrayList(){
        ArrayList ar;
        ar = new ArrayList();
        SimpleDateFormat formatoFecha=new SimpleDateFormat("dd/MM/yyyy");
        ar.add(nombre);
        ar.add(apellidos);
        ar.add(formatoFecha.format(fechaNacim.getTime()));
        return ar;
    }
    
    
}

