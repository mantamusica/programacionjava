/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploligaduradinamicaempleado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author usuario
 */
public class Jefe extends Empleado {

    private String nomDeparJefe;

    public Jefe() {
    }

    public Jefe(String nombre, int edad, String nomDeparJefe) {
        super(nombre, edad);
        this.nomDeparJefe = nomDeparJefe;
    }

    @Override
    public void PedirDatos() {
        InputStreamReader via = new InputStreamReader(System.in);
        BufferedReader teclado = new BufferedReader(via);
        super.PedirDatos();
        try {
            System.out.println("\n\tDepartamento del que es jefe: ");
            nomDeparJefe = teclado.readLine();
        } catch (IOException ioe) {
            System.out.println("ERROR");
        }
    }

    @Override
    public void mostrarDatos() {
        super.mostrarDatos();
        System.out.println("\n\tNOMBRE DEPARTAMENTO DEL QUE ES JEFE: " + nomDeparJefe);
    }
}
