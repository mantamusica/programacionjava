/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploligaduradinamicaempleado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author usuario
 */
public class Empleado {

    private String nombre;
    private int edad;

    public Empleado() {
    }

    public Empleado(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    
    
    
    
    
    public void PedirDatos() {
        InputStreamReader via = new InputStreamReader(System.in);
        BufferedReader teclado = new BufferedReader(via);
        try {
            System.out.println("\nVamos a insertar los datos del empleado:");
            System.out.println("\n\tNOMBRE: ");
            nombre = teclado.readLine();
            System.out.println("\n\tEDAD: ");
            edad = Integer.parseInt(teclado.readLine());
        } catch (NumberFormatException e) {
            System.out.println("No ha insertado un n�mero entero");
        } catch (IOException ioe) {
            System.out.println("ERROR");
        }
    }

    public void mostrarDatos() {
        System.out.println("\n\nSus datos son: ");
        System.out.println("\n\n\tNOMBRE: " + nombre);
        System.out.println("\n\n\tEDAD: " + edad);
    }
}
