/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploligaduradinamicaempleado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        InputStreamReader flujo = new InputStreamReader(System.in);
        BufferedReader teclado = new BufferedReader(flujo);
        Empleado emp = null;
        int opcion;
        ArrayList<Empleado> empresa = new ArrayList();
        do {
            System.out.println("Elija tipo de empleado:\n"
                    + "1-Empleado b�sico\n"
                    + "2-Jefe\n"
                    + "3-Becario\n"
                    + "4-Salir");
            opcion = Integer.parseInt(teclado.readLine());
            switch (opcion) {
                case 1:
                    emp = new Empleado();
                    break;
                case 2:
                    emp = new Jefe();
                    break;
                case 3:
                    emp = new Becario();
                    break;
            }
            if (opcion == 1 | opcion == 2 | opcion == 3) {
                emp.PedirDatos();
                empresa.add(emp);
            }
        } while (opcion != 4);
        for (Empleado empleado : empresa) {
            empleado.mostrarDatos();
        }
    }
}
