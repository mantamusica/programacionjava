/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploligaduradinamicaempleado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author usuario
 */
public class Becario extends Empleado{
    private String estudios;

    public Becario() {
    }

    public Becario( String nombre, int edad,String estudios) {
        super(nombre, edad);
        this.estudios = estudios;
    }

    
    public void PedirDatos() {
        super.PedirDatos();
        InputStreamReader via = new InputStreamReader(System.in);
        BufferedReader teclado = new BufferedReader(via);
       
        System.out.println("\n\tEstudios: ");
        try {
            estudios=teclado.readLine();
        } catch (IOException ioe) {
            System.out.println("ERROR");
        }
    }
    
    public void mostrarDatos(){
         super.mostrarDatos();
         System.out.println("\n\tESTUDIOS DEL BECARIO: "+estudios);
      }
    
}
