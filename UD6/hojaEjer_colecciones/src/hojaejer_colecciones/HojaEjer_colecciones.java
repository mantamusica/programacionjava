/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hojaejer_colecciones;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author dgf
 */
public class HojaEjer_colecciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LinkedList<Candidato> lista = new LinkedList();
        
        Candidato cand1 = new Candidato("Ana", 53);
        Candidato cand2 = new Candidato("Bea", 53);
        Candidato cand3 = new Candidato("Oliver", 53);
        Candidato cand4 = new Candidato("Leo", 53);
        Candidato cand5 = new Candidato("Lia", 53);
        Candidato cand6 = new Candidato("Anais", 53);
        
        lista.add(cand1);
        lista.add(cand2);
        lista.add(cand3);
        lista.add(cand4);
        
        lista.add(1, cand5);
        
        lista.set(0, cand6);
        lista.remove(cand3);
        
        for (Candidato ObjRecibido : lista) {
            System.out.println(ObjRecibido.toString());
        }
        lista.removeLast();
        
        System.out.println(lista.indexOf(cand5));
        
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).toString());
            
        }
        ListIterator<Candidato> iterator = lista.listIterator();
        
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        List<Candidato> sublista = new LinkedList();
        sublista = lista.subList(1, lista.size());
        
        ArrayList<Candidato> jefes = new ArrayList();
        
        Candidato jefe1 = new Candidato("tomas", 53);
        Candidato jefe2 = new Candidato("teresa", 53);
        jefes.add(jefe1);
        jefes.add(jefe2);
        
        System.out.println("Añadimos al final");
        lista.addAll(lista.size(), jefes);
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).toString());
            
        }
        System.out.println(" ");
        //cola
        System.out.println(lista.getFirst());
        lista.removeFirst();
        System.out.println(lista.getFirst());
        lista.addLast(cand1);
        
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).toString());
        }
        System.out.println(" ");
            //pila
        
        System.out.println(lista.getLast());
        lista.removeLast();
        System.out.println(lista.getLast());
        lista.addFirst(cand2);
        
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).toString());
        }
        System.out.println("\n otro ejercico");
        ArrayList frase = new ArrayList();
        frase.add("Hello, ");
        frase.add("How ");
        frase.add("are ");
        frase.add("you?");
        for (int i = 0; i < frase.size(); i++) {
            System.out.print(frase.get(i));
        }
        
        System.out.println("\n otro ejercico");
        ArrayList num = new ArrayList();
        num.add(10);
        num.add(20);
        num.add(20);
        num.add(40);
        int total=0;
        for (int i = 0; i < num.size(); i++) {
            total=(int)num.get(i)+total;
        }
        System.out.println(total);
    }
    userinput;
}
