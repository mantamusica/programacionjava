package colec;
import java.util.HashSet;
import java.util.Iterator;


public class Principal {

    public static void main(String[] args) {
        Alumno alumno1=new Alumno();
        Alumno al1=new Alumno("Ángel",60);
        Alumno al2=new Alumno("Ángel",45);
        Alumno al3=new Alumno("Ángel",60);
        Alumno al4=new Alumno("Nieves",60);
        Alumno al5=new Alumno("Ángel",60);
        Alumno al6=new Alumno("Nieves",58);
        Alumno al7=new Alumno("Nieves",60);
        Alumno al8=new Alumno("Nieves",42);
        Alumno al9=new Alumno("Dolores",50);
        HashSet  colec= new HashSet();
        colec.add(al1);//al solo pasarles un codigo solo no hace falta crear el objeto
        colec.add(al2);
        colec.add(al3);
        colec.add(al4);
        colec.add(al5);
        colec.add(al6);
        colec.add(al7);
        colec.add(al8);
        colec.add(al9);
        Iterator iterator=colec.iterator();
        Alumno obj= null;
         while (iterator.hasNext()){
             obj=(Alumno)iterator.next();
             System.out.println("la coleccion es "+obj );
    }
     
        
        
    }
        
        
       
}
