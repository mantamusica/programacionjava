package colec;
public class Alumno {
    String nombre;
    int edad;

    public Alumno() {
    }

    public Alumno(String nombre, int edad) {
        this.nombre=nombre;
        this.edad=edad;
    }

    @Override
    public String toString() {
        return "Alumno{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }
    @Override
     public boolean equals (Object obj){
        Alumno a=(Alumno) obj;
        return a.nombre.equals(nombre)&& a.edad==edad;
        
    }
    
    @Override
    public int hashCode(){
       return nombre.hashCode();
    }
   
        
    
}
    

  
    
    
    

