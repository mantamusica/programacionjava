package colec.tree;

import colec.*;

public class Alumno implements Comparable<Alumno> {
    String nombre;
    int edad;

    public Alumno() {
    }

    public Alumno(String nombre, int edad) {
        this.nombre=nombre;
        this.edad=edad;
    }

    @Override
    public String toString() {
        return "Alumno{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }

   
    @Override
     public int compareTo(Alumno o){
      int comparacion=nombre.compareTo(o.nombre);
      if(comparacion==0){
          return(edad-o.edad);
      }
      else return comparacion;
     }   
    
}
    

  
    
    
    

