/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;



import java.io.*;
import java.net.*;


public class Cliente {

    public static void main(String[] args) {
        Boolean Salir=false;
        Leer leer=new Leer();
        try {
            Socket canalComunicacion = null;
            OutputStream bufferSalida;
            DataOutputStream datos;
            canalComunicacion = new Socket("192.168.0.73", 10831);
            bufferSalida = canalComunicacion.getOutputStream();
            datos = new DataOutputStream(bufferSalida);
            String mensaje = leer.Cadena("Introduce una palabra para enviar");
            while (Salir == false){
                datos.writeUTF(mensaje);
                datos.writeUTF(" ");
            }
            datos.close();
            bufferSalida.close();
            canalComunicacion.close();
        } catch (UnknownHostException ex) {
           System.out.println("Error");
        } catch (IOException ex) {
            System.out.println("Error");
        }

    }
}
