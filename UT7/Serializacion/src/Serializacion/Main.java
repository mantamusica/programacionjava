package Serializacion;
import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Angel Guerra
 */
public class Main {

    
    public static void Escribir(){
        Scanner t = new Scanner(System.in);
        
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        
        String nom;
        long telef;
        
        try {
            fos = new FileOutputStream("C:/Users/usuario/Desktop/Serializacion.txt", true);
            oos = new ObjectOutputStream(fos);
            
            System.out.println("Nombre: ");
            nom = t.nextLine();
            System.out.println("Telefono: ");
            telef = t.nextInt();
            
            Constructor cons = new Constructor(nom, telef);
            
            oos.writeObject(cons);
        }catch (IOException io) {
            System.err.println(io.getMessage());
        }finally{
            if(oos!=null)try{
                oos.close();
            }catch(IOException io){
                System.err.println(io.getMessage());
            }
        }
    }
    
    
    public static void Leer (){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        boolean fin = false;
        
        try{
            fis = new FileInputStream("C:/Users/usuario/Desktop/Serializacion.txt");
            ois = new ObjectInputStream(fis);
            while (!fin){
                Constructor a = null;
                
                a = (Constructor)ois.readObject();
                System.out.println("Nombre: "+a.nombre);
                System.out.println("Telefono: "+a.tfno);
            }
        }catch(EOFException eof){
            fin = true;
        }catch(IOException io){
            System.err.println(io.getMessage());
        }catch(ClassNotFoundException cnfe){
            System.err.println(cnfe.getMessage());
        }finally{
            try {
                ois.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Escribir();
        Leer();
    }
}
