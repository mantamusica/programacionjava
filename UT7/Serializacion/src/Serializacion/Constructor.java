package Serializacion;
/**
 * @author Angel Guerra
 * @since 27 - Marzo - 2012
 */
public class Constructor implements java.io.Serializable{
    public String nombre;
    public long tfno;
    
    /**
     * @param n
     * @param t 
     */
    public Constructor(String n, long t){
        nombre = n;
        tfno = t;
    }
}
