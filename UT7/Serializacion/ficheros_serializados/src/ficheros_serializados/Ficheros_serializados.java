
package ficheros_serializados;

import java.util.Scanner;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ficheros_serializados {
    public static void escribir (){
        Scanner teclado=new Scanner(System.in);
        
        FileOutputStream f=null;
        ObjectOutputStream os=null;
        
        String nomb;
        long tlfno;

        try{
            f=new FileOutputStream ("amigos.txt",true);
            os=new ObjectOutputStream (f);
            
            System.out.println("Nombre de un amigo ");
            nomb=teclado.nextLine();
            System.out.println("Telefono ");
            tlfno=teclado.nextLong();
            
            Amigo a =new Amigo(nomb,tlfno);
            
            System.out.println("Amigo "+ a.nombre+ "con telefono "+ a.telefono+"dado de alta");
            os.writeObject(a);
            
            if(os!=null){
                os.close();
                f.close();
            }
        }catch (IOException e){
            System.out.println("Error en escritura");
        }
    }
    
    public static void leer() {
        FileInputStream f=null;
        ObjectInputStream os =null;
        boolean fin=false;
        
        try{
            f=new FileInputStream("amigos.txt");
            os=new ObjectInputStream(f);
            
            while(!fin){
                Amigo a=null;
                
                a=(Amigo)os.readObject();
                
                System.out.println("Amigo "+a.nombre);
                System.out.println("Telefono "+a.telefono);
            }
        }catch (EOFException eof){
            fin=true;
        }
        catch (ClassNotFoundException cnf){
            System.out.println("Error clase");
        }
        catch (IOException e){
            System.out.println("Error lectura");
        }finally{
            if (os != null) {
                try {
                    os.close();
                } catch (IOException ex) {
                    Logger.getLogger(Ficheros_serializados.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
             
    }
    public static void main(String[] args) {
        escribir();
        leer();
    }
}
