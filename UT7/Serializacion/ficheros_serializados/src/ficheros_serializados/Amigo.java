
package ficheros_serializados;


public class Amigo implements java.io.Serializable{
    public  String nombre;
    public long telefono;
    
    public Amigo(String nom, long telf){
        nombre=nom;
        telefono=telf;
    }
}