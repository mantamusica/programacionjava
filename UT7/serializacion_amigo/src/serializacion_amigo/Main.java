/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package serializacion_amigo;


import java.io.*;
import java.util.Scanner;

/**
 *
 * @author Cristina
 */
public class Main {
    public static int leeOpcion() {

        int nOpcion = 10 ;

        try {
            BufferedReader lect = new BufferedReader(new InputStreamReader(System.in));

            // Leer desde teclado la opción
            String cOpcion = lect.readLine();

            nOpcion = Integer.parseInt(cOpcion) ;
         } catch(NumberFormatException e) {
              System.err.println("NO ES UN NÚMERO VÁLIDO: Vuelve a intentarlo.");}
           catch(IOException ex) {
               System.err.println("NO ES UN NÚMERO VÁLIDO: Vuelve a intentarlo.");}
           catch(Exception exc) {
               System.err.println("NO ES UN NÚMERO VÁLIDO: Vuelve a intentarlo.");}

        return (nOpcion) ;
    }

    public static void borrarFichero(String fichero) {
        // Attempt to delete it
        File f = new File(fichero);
        boolean success = f.delete();
        if (success){
            //System.out.println(" ");
            System.out.println("\n Borrado ok") ;
        }
        else{
            //System.out.println(" ");
            System.out.println("\n No se puede borrar el fichero "+fichero) ;
        }
            
    }

    // Comprueba si hay algún registro en el fichero
    private static boolean tieneRegistros(String fichero){
        boolean tiene = false ;
        FileInputStream fiAmigLec ;
        ObjectInputStream fluent;
        Amigo miamigo = null ;
        try {
            fiAmigLec = new FileInputStream(fichero);
            fluent = new ObjectInputStream(fiAmigLec);

            // Leer el objeto del fichero
            miamigo = (Amigo) fluent.readObject();

            if (miamigo != null)
                tiene = true ;
            fiAmigLec.close();
            fluent.close();
        } catch (FileNotFoundException e) {
            System.out.println( "Error: " + e.getMessage() ) ;
        } catch (Exception ex) {
            System.out.println( "Capturada excepción en tieneRegistros()" );
        }

        return(tiene) ;
    }

    public static String convertir_dni(int dni){
        String nif=null;
        char [] letranif ={'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};

        nif=String.valueOf(dni);
        nif=nif+letranif[dni%23];


        
        return nif;
    }

    public static void escribir (String fichero){
        String nombres_amigos;
        long telefono;
        String nif;
        Scanner e= new Scanner(System.in);
        BufferedReader lect=new BufferedReader(new InputStreamReader(System.in));
        //int opc;
        //char resp='s';
        
        FileOutputStream fs;
        ObjectOutputStream os;// = new ObjectOutputStream(fs);

        //int c;

        try{
            fs = new FileOutputStream(fichero,true);

            
                System.out.print("\n\tNIF ->");
                nif=convertir_dni(e.nextInt());
                e.nextLine();
                System.out.print("\n\tNombre amigo -> ");
                nombres_amigos=e.nextLine();

                System.out.print("\n\tTelefono -> ");
                telefono=e.nextLong();

                Amigo a =new Amigo(nombres_amigos,nif,telefono);

                //comprobar si tiene elementos ya el fichero
                if (tieneRegistros(fichero))
                    // Uso esta redefinición de la clase para
                    // evitar que escriba datos de la cabecera que corromperían
                    // la posterior lectura del fichero
                    os = new MiObjectOutputStream(fs);
                else
                    os = new ObjectOutputStream(fs);

                os.writeObject(a);

            if (os!=null){
                os.close();
                fs.close();
            }

        }catch(IOException ex){
            System.out.println("\nError escritura");
        }
    }

    public static void leer(String fichero) throws ClassNotFoundException{
        FileInputStream fs = null;
        ObjectInputStream os =null;
        boolean eof_fich=false;
        int cont=1;

        try{
            fs=new FileInputStream(fichero);
            os=new ObjectInputStream(fs);

            System.out.println(" ");
            while(eof_fich!=true){
                Amigo a=null;
                a=(Amigo)os.readObject();
                System.out.println(cont+"--> "+a.nombre+" "+a.NIF+"  "+a.telefono);
                cont++;
            }
        }catch(EOFException eeof){
            eof_fich=true;
        }catch(IOException ex){
            System.out.println("Error leer");
        }
    }

    public  static void buscaAmigo(String fichero) {
        Amigo miamigo = null ;
        boolean salir = false ;
        String nif = null;
        boolean encontrado =false;

        FileInputStream fiAmigLec ;
        ObjectInputStream fluent ;

        try {
            BufferedReader lect = new BufferedReader(new InputStreamReader(System.in));

            // Leer desde teclado el nif
            
            System.out.print("\nNIF amigo a buscar con letra:  ");
            nif = lect.readLine();
            

            fiAmigLec = new FileInputStream( fichero );
            fluent = new ObjectInputStream(fiAmigLec);

            do {
                // Leer el objeto del fichero
                try {
                    miamigo = (Amigo) fluent.readObject();
                } catch (Exception ex) {
                    salir = true ;
                }
                // Si hemos leído un objeto
                if (miamigo != null)  {
                    // si el nif se corresponde con el que buscamos
                    if (miamigo.NIF.equals(nif.toUpperCase())){
                        
                        System.out.println("\nAmigo encontrado ->"+miamigo.nombre+
                                " "+miamigo.NIF+" "+miamigo.telefono);
                        salir=true;
                        encontrado=true;
                    }

                }
                else
                    salir = true ;

            } while ((miamigo != null) && (salir == false)) ;

            if (encontrado == false){
                
                System.out.println("\nAmigo no encontrado");

            }
            
            // Cerrar
            fiAmigLec.close() ;
            fluent.close();
        } catch (FileNotFoundException e) {
            System.out.println( e.getMessage() ) ;
        } catch (Exception ex) {
            System.out.println( ex.getMessage() ) ;
            System.out.println( "Capturada" ) ;
        }finally {
            // Actividades que siempre ocurren
        }
    }

        public  static void buscaAmigoSin(String fichero) {
        Amigo miamigo = null ;
        boolean salir = false ;
        String nif = null;
        boolean encontrado = false;

        FileInputStream fiAmigLec ;
        ObjectInputStream fluent ;

        try {
            BufferedReader lect = new BufferedReader(new InputStreamReader(System.in));

            // Leer desde teclado el nif
            System.out.print("\nNIF amigo a buscar sin letra:  ");
            nif = lect.readLine();

            fiAmigLec = new FileInputStream( fichero );
            fluent = new ObjectInputStream(fiAmigLec);

            do {
                // Leer el objeto del fichero
                try {
                    miamigo = (Amigo) fluent.readObject();
                } catch (Exception ex) {
                    salir = true ;
                }
                // Si hemos leído un objeto
                if (miamigo != null)  {
                    // si el nif se corresponde con el que buscamos
                    
                    if (miamigo.NIF.substring(0, (miamigo.NIF.length()-1)).equals(nif)){
                        
                        System.out.println("\nAmigo encontrado ->"+miamigo.nombre+
                                " "+miamigo.NIF+" "+miamigo.telefono);
                        salir=true;
                        encontrado=true;
                    }

                }
                else
                    salir = true ;

            } while ((miamigo != null) && (salir == false)) ;

            if (encontrado == false){
                
                System.out.println("\nAmigo no encontrado");
            }
            // Cerrar
            fiAmigLec.close() ;
            fluent.close();
        } catch (FileNotFoundException e) {
            System.out.println( e.getMessage() ) ;
        } catch (Exception ex) {
            System.out.println( ex.getMessage() ) ;
            System.out.println( "Capturada" ) ;
        }finally {
            // Actividades que siempre ocurren
        }
    }

    public static void borrarAmigo(String fichero){
        boolean flag=false;

        File ruta=new File(fichero);
        FileInputStream fiAmigLec=null;
        ObjectInputStream flujo=null;

        File ruta1=new File("Aux.dat");
        FileOutputStream fichero1=null;
        ObjectOutputStream flujo1=null;

        Scanner teclado=new Scanner(System.in);
        System.out.print(" Nif amigo a quitar sin letra:  ");
        String quitar=teclado.nextLine();
        

        Amigo miamigo=null;

        try{
            fiAmigLec=new FileInputStream(ruta);
            flujo=new ObjectInputStream(fiAmigLec);

            fichero1=new FileOutputStream(ruta1, true);
            flujo1=new ObjectOutputStream(fichero1);


            String nombre;
            long telefono;
            String nif;

            while(!flag){
                try {
                    miamigo=(Amigo)flujo.readObject();
                } catch (ClassNotFoundException ex) {
                    System.out.println("Clase no existente");
                }
                nombre=miamigo.nombre;
                telefono=miamigo.telefono;
                nif=miamigo.NIF;

                if(nif.substring(0,(nif.length()-1)).compareTo(quitar)!=0){
                    flujo1.writeObject(miamigo);
                }
                else{
                    System.out.println("\n Amigo borrado");
                }
            }
        }
        catch (EOFException eof){
            flag=true;
        }
        catch (IOException e) {
            System.out.println("Error de lectura o escritura");
        }
        finally{
            try{
                flujo.close();
                flujo1.close();
                boolean success=ruta.delete();
                if(success){
                    System.out.println("\n Borrado completo y con exito");
                }
                else{
                    System.out.println("\n Imposible borrar");
                }
                
                ruta1.renameTo(ruta);

            } catch (IOException ex) {
                System.out.println("Error al cerrar el fichero");
            }
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        String fichero ="amigos.txt";

        boolean seguir = true ;

        while (seguir) {

            
            System.out.println("\nSelecciona una opción:");

            System.out.println("[1] Añadir amigo.");
            System.out.println("[2] Listar amigo.");
            System.out.println("[3] Buscar amigo por NIF con letra.");
            System.out.println("[4] Buscar amigo por NIF sin le2tra.");
            System.out.println("[5] Borrar amigo.");
            System.out.println("[6] Borrar fichero.");
            System.out.println("[7] Salir.");

            System.out.println("Escriba la selección: ");
            int selec = 0 ;
            selec  = leeOpcion() ;

            // Según la selección que se haya realizado
            switch (selec){
                case 1: escribir(fichero);
                    break;
                case 2: leer(fichero);
                    break;
                case 3: buscaAmigo(fichero) ;
                    break;
                case 4: buscaAmigoSin(fichero) ;
                    break;
                case 5: borrarAmigo(fichero);
                    break;
                case 6: borrarFichero(fichero);
                    break;
                case 7:
                    seguir = false ;
                    break ;
                default:
                    System.out.println("Selecciona una opción válida.");
            }
        }
        
    }
}
