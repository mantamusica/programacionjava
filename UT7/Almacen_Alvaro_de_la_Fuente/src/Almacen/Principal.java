/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Almacen;

import java.io.File;
import java.io.IOException;



public class Principal {

 
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Leer leer=new Leer();
        File fich = new File("almacen.dat");
  
        

        boolean salir = false;
        int op;

        do {
            System.out.println("1.Cargar articulo");
            System.out.println("2.Visualizar contenido");
            System.out.println("3.Pedidos necesarios de realizar");
            System.out.println("4.Cantidad articulos distintos en el almacen");
            System.out.println("5.Total articulos+existencias");
            System.out.println("6.Modificar el numero de existencias ");
            System.out.println("7.Borrar articulo");
            System.out.println("8.Copia seguridad");
            System.out.println("9.visualizar copia seguridad");
            System.out.println("10.Salir");

            switch (leer.Entero("Introdude una opcion")) {

                case 1:
                    Fichero.grabarArticulo(fich);
                    break;
                case 2:
                    Fichero.listarContenido(fich);
                    break;
                case 3:
                    Fichero.pedidosNecesarios(fich);
                    break;
                case 4:
                    //Fichero.(fich);
                    break;
                case 5:
                    //Fichero.(fich);
                    break;
                case 6:
                    //Fichero.(fich);
                    break;
                case 7:
                    Fichero.borrarArticulos(fich);
                    break;
                case 8:
                    Fichero.crearCopiaseguridad(fich);
                    break;
                case 9:
                    break;
                case 10:
                    salir = true;
                    break;
                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (salir == false);

    }
}
