/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Almacen;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
public class Fichero {
    static void grabarArticulo(File fich) throws IOException {

        Leer leer = new Leer();
        String codigo, descripcion;
        int existencias=0;

        codigo= leer.Cadena("Introduce codigo");
        descripcion= leer.Cadena("Introduce descripcion");
        do {
            existencias=leer.Entero("Introduce existencias");
        }while((existencias<-1) | (existencias >1000));
        
        

        Articulo articulos = new Articulo(codigo,descripcion,existencias);

        FileOutputStream fos;
        ObjectOutputStream oos;

        if (fich.exists()) {
            fos = new FileOutputStream(fich, true);
            oos = new MiObjectOutputStream(fos);
        } else {
            fos = new FileOutputStream(fich);
            oos = new ObjectOutputStream(fos);
        }

        oos.writeObject(articulos);
        oos.close();
        fos.close();
    }
    static void listarContenido(File fich) throws IOException, ClassNotFoundException {

        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            try {
                while (true) {
                    Articulo articulos = (Articulo) ois.readObject();
                    System.out.println(articulos);
                }

            } catch (EOFException eof) {
                
            }

            fis.close();
            ois.close();

        } else {
            System.out.println("No existen clientes");
        }
    }
    static void borrarArticulos(File fich) throws IOException, ClassNotFoundException {
        Leer leer = new Leer();
        File demo = new File("demo.txt");
        boolean encontrado = false;

        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            FileOutputStream fos;
            ObjectOutputStream oos;

            String codigo;
            codigo = leer.Cadena("Introduce codigo a borrar");

            try {
                while (true) {
                    Articulo articulos = (Articulo) ois.readObject();

                    if (!articulos.getCodigo().equals(codigo)) {

                        if (demo.exists()) {
                            fos = new FileOutputStream(demo, true);
                            oos = new MiObjectOutputStream(fos);
                        } else {
                            fos = new FileOutputStream(demo);
                            oos = new ObjectOutputStream(fos);
                        }

                        oos.writeObject(articulos);
                        oos.close();
                        fos.close();

                    } else {
                        encontrado = true;
                    }
                }

            } catch (EOFException eof) {
                ois.close();
                fis.close();

                if (encontrado == true) {
                    System.out.println("Cliente borrado");
                } else {
                    System.out.println("No se encontro el cliente");
                }
            }

            fich.delete();
            demo.renameTo(fich);
        } else {
            System.out.println("No existe el fichero");
        }
    }
        static void pedidosNecesarios(File fich) throws FileNotFoundException, IOException, ClassNotFoundException{
            boolean encontrado=false;
            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);
            try{
                while(true){
                    Articulo ar=(Articulo) ois.readObject();
                    if(ar.getExistencias()<10){
                        System.out.println(ar.getCodigo());
                        encontrado=true;
                        
               }
                }
               }catch (EOFException eof){
                            
               }
            ois.close();
            fis.close();
            
            if (encontrado!=true){
                System.out.println("No es necesario realizar pedidos");
        }
        }
    

        static void crearCopiaseguridad(File fich) throws IOException {
        Leer leer = new Leer();
        String nombre = leer.Cadena("Que nombre desea poner al fichero");

        if (fich.exists()) {
             File fich2 = new File(nombre + ".txt");
            fich.renameTo(fich2);
        } else {
            System.out.println("no existe");
        }

    }
        
}
