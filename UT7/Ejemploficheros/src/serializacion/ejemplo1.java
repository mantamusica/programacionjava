/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion;

import java.io.*;

/**
 *
 * @author dgf
 */
public class ejemplo1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        String nombre;
        int calificacion;
        Leer leer = new Leer();

        FileOutputStream fos = new FileOutputStream("fichero.txt", true);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        nombre = leer.Cadena("Introduce nombre");
        calificacion = leer.Entero("Introduce calificacion");
        Alumno alumno = new Alumno(nombre, calificacion);

        FileInputStream fis = new FileInputStream("fichero.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);

        oos.writeObject(alumno);

        try {
            while (true) {
                Alumno pepe = (Alumno) ois.readObject();
                System.out.println(pepe);
            }
        } catch (EOFException eof) {
            System.out.println("Final de fichero");
        }

    }

}
