/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio7;

import java.io.*;
import java.nio.file.Files;

/**
 *
 * @author dgf
 */
public class Ficheros {

    static void grabarClientes(File fich) throws IOException {

        Leer leer = new Leer();
        String nif, nombre, telefono, direccion;
        int deuda;

        nif = leer.Cadena("Introduce nif");
        nombre = leer.Cadena("Introduce nombre");
        telefono = leer.Cadena("Introduce telefono");
        direccion = leer.Cadena("Introduce direccion");
        deuda = leer.Entero("Introduce deuda");

        Clientes cliente = new Clientes(nif, nombre, telefono, direccion, deuda);

        FileOutputStream fos;
        ObjectOutputStream oos;

        if (fich.exists()) {
            fos = new FileOutputStream(fich, true);
            oos = new MiObjectOutputStream(fos);
        } else {
            fos = new FileOutputStream(fich);
            oos = new ObjectOutputStream(fos);
        }

        oos.writeObject(cliente);
        oos.close();
        fos.close();
    }

    static void listarClientes(File fich) throws IOException, ClassNotFoundException {

        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            try {
                while (true) {
                    Clientes cliente = (Clientes) ois.readObject();
                    System.out.println(cliente);
                }

            } catch (EOFException eof) {
                
            }

            fis.close();
            ois.close();

        } else {
            System.out.println("No existen clientes");
        }
    }

    static void buscarClientes(File fich) throws IOException, ClassNotFoundException {
        String nif;
        boolean encontrado = false;

        if (fich.exists()) {
            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            Leer leer = new Leer();

            nif = leer.Cadena("Introduce NIF");

            try {
                while (true) {
                    Clientes cliente = (Clientes) ois.readObject();

                    if (cliente.getNIF().equals(nif)) {
                        System.out.println(cliente.toString());
                        encontrado = true;
                    }
                }

            } catch (EOFException eof) {
                if (!encontrado) {
                    System.out.println("No existe el cliente");
                }

            }

            ois.close();
        } else {
            System.out.println("No existe el fichero");
        }

    }

    static void borrarClientes(File fich) throws IOException, ClassNotFoundException {
        Leer leer = new Leer();
        File demo = new File("demo.txt");
        boolean encontrado = false;

        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            FileOutputStream fos;
            ObjectOutputStream oos;

            String nif;
            nif = leer.Cadena("Introduce NIF a borrar");

            try {
                while (true) {
                    Clientes cliente = (Clientes) ois.readObject();

                    if (!cliente.getNIF().equals(nif)) {

                        if (demo.exists()) {
                            fos = new FileOutputStream(demo, true);
                            oos = new MiObjectOutputStream(fos);
                        } else {
                            fos = new FileOutputStream(demo);
                            oos = new ObjectOutputStream(fos);
                        }

                        oos.writeObject(cliente);
                        oos.close();
                        fos.close();

                    } else {
                        encontrado = true;
                    }
                }

            } catch (EOFException eof) {
                ois.close();
                fis.close();

                if (encontrado == true) {
                    System.out.println("Cliente borrado");
                } else {
                    System.out.println("No se encontro el cliente");
                }
            }

            fich.delete();
            demo.renameTo(fich);
        } else {
            System.out.println("No existe el fichero");
        }

    }

    static void borrarFichero(File fich) {
        if (fich.exists()) {
            fich.delete();
            System.out.println("Fichero Borrado");
        } else {
            System.out.println("No existe el fichero");
        }

    }

    static File renombrarFichero(File fich) {  //buscar problema
        Leer leer = new Leer();
        String nombre;

        if (fich.exists()) {
            nombre = leer.Cadena("Que nombre desea poner al fichero");
            File fich2 = new File(nombre + ".data");
            fich.renameTo(fich2);
            fich=new File(nombre + ".data");
        } else {
            System.out.println("No existe el fichero");
        }
    
        return fich;
    }
}
