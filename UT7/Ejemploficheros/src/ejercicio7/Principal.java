/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio7;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author dgf
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Leer leer = new Leer();
        File fich = new File("clientes.dat");
        

        boolean salir = false;
        int op;

        do {
            System.out.println("1	Añadir cliente");
            System.out.println("2	Listar clientes");
            System.out.println("3	Buscar clientes");
            System.out.println("4	Borrar cliente");
            System.out.println("5	Borrar fichero de clientes completamente");
            System.out.println("6	Cambiar nombre al fichero");
            System.out.println("7	Salir de la aplicación.");

            switch (leer.Entero("Introdude una opcion")) {

                case 1:
                    Ficheros.grabarClientes(fich);
                    break;
                case 2:
                    Ficheros.listarClientes(fich);
                    break;
                case 3:
                    Ficheros.buscarClientes(fich);
                    break;
                case 4:
                    Ficheros.borrarClientes(fich);
                    break;
                case 5:
                    Ficheros.borrarFichero(fich);
                    break;
                case 6:
                    fich = Ficheros.renombrarFichero(fich);
                    break;
                case 7:
                    salir = true;
                    break;
                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (salir == false);

    }
}
