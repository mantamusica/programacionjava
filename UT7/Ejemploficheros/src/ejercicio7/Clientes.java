/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio7;

import java.io.Serializable;

/**
 *
 * @author dgf
 */
public class Clientes implements Serializable {
    
    private String NIF,Nombre,Telefono,Direccion;
    private int Deuda;

    public Clientes(String NIF, String Nombre, String Telefono, String Direccion, int Deuda) {
        this.NIF = NIF;
        this.Nombre = Nombre;
        this.Telefono = Telefono;
        this.Direccion = Direccion;
        this.Deuda = Deuda;
    }

    public String getNIF() {
        return NIF;
    }

    public void setNIF(String NIF) {
        this.NIF = NIF;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public int getDeuda() {
        return Deuda;
    }

    public void setDeuda(int Deuda) {
        this.Deuda = Deuda;
    }

    @Override
    public String toString() {
        return "Clientes{" + "NIF=" + NIF + ", Nombre=" + Nombre + ", Telefono=" + Telefono + ", Direccion=" + Direccion + ", Deuda=" + Deuda + '}';
    }
    
    
    
    
    
    
    
    
    
    
    
}


