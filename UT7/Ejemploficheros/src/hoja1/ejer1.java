/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hoja1;

import java.io.*;

/**
 *
 * @author dgf
 */
public class ejer1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {

        String cadena = "Hola";
        int leeByte = 0;
        char caracter;
        Leer leer = new Leer();

        //escribir byte
        FileOutputStream fichEsc = new FileOutputStream("ejemplo4a.txt", true);

        for (int pos = 0; pos < cadena.length(); pos++) {
            fichEsc.write((byte) cadena.charAt(pos));
        }

        //leer byte
        FileInputStream fichLec = new FileInputStream("ejemplo4a.txt");

        while (leeByte != -1) {
            caracter = (char) leeByte;
            System.out.print(caracter);
            leeByte = fichLec.read();
        }
        leeByte = 0;

        //añadir mas bytes
        fichEsc.write(' ');
        fichEsc.write('1');
        fichEsc.write('2');
        fichEsc.write(' ');

        //leer bytes
        while (leeByte != -1) {
            caracter = (char) leeByte;
            System.out.print(caracter);
            leeByte = fichLec.read();
        }
        leeByte = 0;

        /**
         * ************************
         */
        //escribir caracteres( filewriter deja cadenas
        FileWriter fichEsc2 = new FileWriter("ejemplo5a.txt");

        cadena = leer.Cadena("Que desea escribir en el fichero?");
        fichEsc2.write(cadena);
        fichEsc2.close();

        //leer caracteres
        FileReader fichLect2 = new FileReader("ejemplo5a.txt");

        while (leeByte != -1) {
            caracter = (char) leeByte;
            System.out.print(caracter);
            leeByte = fichLect2.read();
        }
        fichLect2.close();

        
        
        

    }

}
