/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hoja1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author dgf
 */
public class buffers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String cadena;
        Leer leer=new Leer();
        
        cadena=leer.Cadena("Introduce palabra a escribir");

          
       //escribir cadena con buffer      
        FileWriter fichero_w = new FileWriter("ejemplo6a.txt");
        BufferedWriter flujo_esc = new BufferedWriter(fichero_w);
        flujo_esc.write(cadena);
        flujo_esc.flush();
        
     
         //leer cadena con buffer   
        FileReader fichero_r = new FileReader("ejemplo6a.txt");
        BufferedReader flujo = new BufferedReader(fichero_r);
        cadena = flujo.readLine();
        System.out.println(cadena);
        flujo.close();
        
    }
    
}
