/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo7;

import java.io.*;

/**
 *
 * @author dgf
 */
public class Principla {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        Leer leer = new Leer();
        String nombre;
        int edad;

        nombre = leer.Cadena("introduce nombre");
        edad = leer.Entero("introduce edad");

        Alumno alu = new Alumno(nombre, edad);

        FileOutputStream fichEsc = new FileOutputStream("fichero.txt"); //guarda flujo de bytes en fichero
        ObjectOutputStream flusal = new ObjectOutputStream(fichEsc); //convierte objeto en flujo de bytes
        flusal.writeObject(alu);

        FileInputStream fichLect = new FileInputStream("fichero.txt");
        ObjectInputStream fluent = new ObjectInputStream(fichLect);
        alu = (Alumno) fluent.readObject();
        System.out.println(alu);

    }

}
