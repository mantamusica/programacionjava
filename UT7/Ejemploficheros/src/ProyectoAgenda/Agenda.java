/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectoAgenda;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author dgf
 */
public class Agenda  {

    private ArrayList<Contacto> agenda = new ArrayList();
    private File fich = new File("datos.dat");
    Leer leer = new Leer();
    
    public Agenda() {
  
    }

    public ArrayList<Contacto> getAgenda() {
        return agenda;
    }

    public void setAgenda(ArrayList<Contacto> agenda) {
        this.agenda = agenda;
    }

    public void anadirContacto() {
        String nombre, telefono, correo;

        nombre = leer.Cadena("introduce nombre");
        telefono = leer.Cadena("introduce telefono");
        correo = leer.Cadena("introduce correo");

        Contacto cont = new Contacto(nombre, telefono, correo);
        agenda.add(cont);
    }

    public void buscarContacto() {

        String nombre;
        boolean encontrado = false;

        nombre = leer.Cadena("Introduce nombre de contacto a buscar");

        for (Contacto con : agenda) {
            if (con.getNombre().equalsIgnoreCase(nombre)) {
                System.out.println(con.toString());
                encontrado=true;
            }
        }
        if (!encontrado) {
            System.out.println("Contacto no encontrado");
        }

    }

    public void eliminarContacto() {

        String nombre;
        boolean encontrado = false;

        nombre = leer.Cadena("Introduce nombre de contacto a buscar");

        Iterator<Contacto> iterator = agenda.iterator();

        while (iterator.hasNext()) {

            if (iterator.next().getNombre().equalsIgnoreCase(nombre)) {
                iterator.remove();
                encontrado = true;
                System.out.println("Contacto borrado");
            }
        }
        if (!encontrado) {
            System.out.println("Contacto no encontrado");
        }

    }

    public void visualizarAgenda() {

        if (agenda.size() > 0) {
            for (Contacto con : agenda) {
                System.out.println(con.toString());
            }
        } else {
            System.out.println("No existen contactos que mostrar");
        }
    }

    public void numeroContactos() {
        int contador = 0;
        if (agenda.size() > 0) {
            for (Contacto con : agenda) {
                contador++;
            }
        } else {
            System.out.println("No existen ningun contacto");
        }
        System.out.println("El numero de contactos es: "+contador);
    }

    public void leeFichero() throws FileNotFoundException, IOException, ClassNotFoundException {
        
        if (fich.exists()) {
            FileInputStream fichLect = new FileInputStream(fich);
            ObjectInputStream fluent = new ObjectInputStream(fichLect);
            agenda = (ArrayList) fluent.readObject();
            fluent.close();
            fichLect.close();
        }

    }

    public void guardarEnFichero() throws FileNotFoundException, IOException {

        FileOutputStream fichEsc = new FileOutputStream(fich);
        ObjectOutputStream flusal = new ObjectOutputStream(fichEsc);
        flusal.writeObject(agenda);
        flusal.close();
        fichEsc.close();
    }

}
