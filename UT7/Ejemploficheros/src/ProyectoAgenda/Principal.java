/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectoAgenda;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author dgf
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {

        Leer leer = new Leer(); 
        Agenda agenda =new Agenda();
        agenda.leeFichero();
    

        boolean salir = false;
        int op;

        do {
            System.out.println("1	Añadir contacto");
            System.out.println("2	Buscar contacto");
            System.out.println("3	Eliminar contacto");
            System.out.println("4	Visualizar agenda");
            System.out.println("5	Número de contactos de mi agenda");
            System.out.println("6	Salir de la aplicación.");

            switch (leer.Entero("Introdude una opcion")) {

                case 1:
                    agenda.anadirContacto();
                    break;
                case 2:
                    agenda.buscarContacto();
                    break;
                case 3:
                    agenda.eliminarContacto();
                    break;
                case 4:
                    agenda.visualizarAgenda();
                    break;
                case 5:
                    agenda.numeroContactos();
                    break;
                case 6:
                    salir = true;
                    break;
                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }
          
        } while (salir == false);
        agenda.guardarEnFichero();
    }

}
