/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectoAgenda;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author dgf
 */
public class Leer {

    InputStreamReader flujo = new InputStreamReader(System.in);
    BufferedReader teclado = new BufferedReader(flujo);

    public Leer() { //Metodo constructor 

    }

    int Entero(String pregunta) {
        int Entero = 0;
        boolean salir = false;
        String leer;
        do {
            System.out.println(pregunta);
            try {
                leer = teclado.readLine();

                Entero = Integer.parseInt(leer);
                salir = true;
            } catch (NumberFormatException e) {
                System.out.println("Error, no se ha introducido un numero.");
            } catch (IOException e) {

            }
        } while (salir == false);
        return Entero;
    }

    double Doble(String pregunta) {
        double Doble = 0;
        boolean salir = false;
        String leer;
        do {
            System.out.println(pregunta);
            try {
                leer = teclado.readLine();

                Doble = Double.parseDouble(leer);
                salir = true;
            } catch (NumberFormatException e) {
                System.out.println("Error, no se ha introducido un numero.");
            } catch (IOException e) {

            }
        } while (salir == false);
        return Doble;
    }

    String Cadena(String pregunta)  {
        String Cadena = null;
        System.out.println(pregunta);
        try{
        Cadena = teclado.readLine();
        }
          catch (IOException e) {

            }
        return Cadena;
        }
    }


