/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion2;


import java.io.*;

/**
 *
 * @author dgf
 */
public class ejemplo1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        String nombre;
        int calificacion;
        
        Leer leer = new Leer();
        File fich = new File("ejemplo7.txt");

        nombre = leer.Cadena("Introduce nombre");
        calificacion = leer.Entero("Introduce calificacion");
        Alumno alumno = new Alumno(nombre, calificacion);

        FileOutputStream fos;
        ObjectOutputStream oos;

        if (fich.exists()) {
            fos = new FileOutputStream(fich, true);
            oos = new MiObjectOutputStream(fos);
        } else {
            fos = new FileOutputStream(fich);
            oos = new ObjectOutputStream(fos);
        }

        oos.writeObject(alumno);

        FileInputStream fis = new FileInputStream(fich);
        ObjectInputStream ois = new ObjectInputStream(fis);
        try {
            while (true) {
                Alumno pepe = (Alumno) ois.readObject();
                System.out.println(pepe);
            }
        } catch (EOFException eof) {
            System.out.println("Final de fichero");
        }

    }

}
