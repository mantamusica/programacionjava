/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion2;

import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author dgf
 */
public class MetodosAlumno {

    static void grabarAlumno(File fich) throws IOException {
        Leer leer = new Leer();
        String nombre;
        int calificacion;

        nombre = leer.Cadena("Introduce nombre");
        calificacion = leer.Entero("Introduce sueldo");

        Alumno alumno = new Alumno(nombre, calificacion);

        FileOutputStream fos;
        ObjectOutputStream oos;

        if (fich.exists()) {
            fos = new FileOutputStream(fich, true);
            oos = new MiObjectOutputStream(fos);
        } else {
            fos = new FileOutputStream(fich);
            oos = new ObjectOutputStream(fos);
        }

        oos.writeObject(alumno);
        oos.close();
        fos.close();

    }

    static void listarAlumnos(File fich) throws IOException, ClassNotFoundException {

        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);
            try {
                while (true) {
                    Alumno alu = (Alumno) ois.readObject();
                    System.out.println(alu);
                }

            } catch (EOFException eof) {
                System.out.println("Final de fichero");
            }

            fis.close();
            ois.close();
        } else {
            System.out.println("No existe fichero");
        }

    }

    static void borrarFichero(File fich) {
        if (fich.exists()) {
            fich.delete();
            System.out.println("Fichero Borrado");
        } else {
            System.out.println("No existe el fichero");
        }

    }

    static void renombrarFichero(File fich) {
        Leer leer = new Leer();
        String nombre = leer.Cadena("Que nombre desea poner al fichero");

        if (fich.exists()) {
            File fich2 = new File(nombre + ".txt");
            fich.renameTo(fich2);
        } else {
            System.out.println("No existe el fichero");
        }

    }

    static void buscarAlumnos(File fich) throws IOException, ClassNotFoundException {
        String nombre;
        boolean encontrado = false;

        if (fich.exists()) {
            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            Leer leer = new Leer();

            nombre = leer.Cadena("Introduce nombre  buscar");

            try {
                while (true) {
                    Alumno alu = (Alumno) ois.readObject();

                    if (alu.getNombre().equalsIgnoreCase(nombre)) {
                        System.out.println(alu);
                        encontrado = true;
                    }
                }
 

            } catch (EOFException eof) {

            }

            ois.close();
        }

        if (!encontrado) {
            System.out.println("no existe el alumno");
        }
    }

    static void borrarAlumno(File fich) throws IOException, ClassNotFoundException {
        Leer leer = new Leer();
        File demo = new File("demo.txt");

        if(fich.exists()){
            
        FileInputStream fis = new FileInputStream(fich);
        ObjectInputStream ois = new ObjectInputStream(fis);

        FileOutputStream fos;
        ObjectOutputStream oos;

        String nombre;
        nombre = leer.Cadena("Introduce nombre a borrar");

        try {
            while (true) {
                Alumno alu = (Alumno) ois.readObject();

                if (!alu.getNombre().equalsIgnoreCase(nombre)) {

                    if (demo.exists()) {
                        fos = new FileOutputStream(demo, true);
                        oos = new MiObjectOutputStream(fos);
                    } else {
                        fos = new FileOutputStream(demo);
                        oos = new ObjectOutputStream(fos);
                    }

                    oos.writeObject(alu);
                    oos.close();
                    fos.close();

                }
            }

        } catch (EOFException eof) {
            ois.close();
            fis.close();
        }

        fich.delete();
        demo.renameTo(fich);
        }
        else{
            System.out.println("No existe el alumno");
        }

    }

//    static void leeDerFichero(File fich) throws FileNotFoundException, IOException, ClassNotFoundException {
//        ArrayList<Alumno>lista=new ArrayList();
//        FileInputStream fichLect = new FileInputStream(fich);
//        ObjectInputStream fluent = new ObjectInputStream(fichLect);
//        lista = (ArrayList) fluent.readObject();
//        fluent.close();
//
//    }
//
//    static void guardarEnFichero(File fich, ArrayList alumnos) throws FileNotFoundException, IOException {
//
//        FileOutputStream fichEsc = new FileOutputStream(fich);
//        ObjectOutputStream flusal = new ObjectOutputStream(fichEsc);
//        flusal.writeObject(alumnos);
//        flusal.close();
//    }
//    
}
