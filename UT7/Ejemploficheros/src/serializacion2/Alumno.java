/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion2;


import java.io.Serializable;

/**
 *
 * @author dgf
 */
public class Alumno implements Serializable {
    
   private String nombre;
   private int calificacion;

    public Alumno(String nombre, int calificacion) {
        this.nombre = nombre;
        this.calificacion = calificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "Alumno{" + "nombre=" + nombre + ", calificacion=" + calificacion + '}';
    }
   
    
}
