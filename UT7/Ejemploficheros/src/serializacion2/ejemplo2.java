/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author dgf
 */
public class ejemplo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Leer leer = new Leer();
        File fich = new File("pepe.txt");
        ArrayList<Alumno> alumnos=new ArrayList();
        
        boolean salir = false;
        int op;

        do {
            System.out.println("1-Grabar un nuevo alumno");
            System.out.println("2-Listar los alumnos grabados");
            System.out.println("3-Buscar un alumno concreto ");
            System.out.println("4-Borrar el fichero guardado");
            System.out.println("5-Cambiar el nombre al fichero");
            System.out.println("6-Borrar alumno");
            System.out.println("7-Salir");

            switch (leer.Entero("Introdude una opcion")) {

                case 1:
                    MetodosAlumno.grabarAlumno(fich);
                    break;
                case 2:
                    MetodosAlumno.listarAlumnos(fich);
                    break;
                 case 3:
                    MetodosAlumno.buscarAlumnos(fich);
                    break;
                 case 4:
                    MetodosAlumno.borrarFichero(fich);
                    
                    break;                     
                  case 5:
                    MetodosAlumno.renombrarFichero(fich);
                    break;
                      
                  case 6:
                      MetodosAlumno.borrarAlumno(fich);
                      break;

                case 7:
                    salir = true;
                    break;

                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (salir == false);

    }

}
