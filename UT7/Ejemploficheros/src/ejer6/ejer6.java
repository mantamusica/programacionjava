/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author dgf
 */
public class ejer6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException  {
        Leer leer = new Leer();
        File fich = new File("datos.txt");
        ArrayList<Alumno> lis_alu = new ArrayList();

        boolean salir = false;
        int op;

        if (fich.exists()) {
            lis_alu = MetodosAlumno.leeDerFichero(fich);
        }

        do {
            System.out.println("1-Grabar un nuevo alumno");
            System.out.println("2-Listar los alumnos grabados");
            System.out.println("3-Buscar un alumno concreto ");
            System.out.println("4-Borrar alumno");
            System.out.println("5-Cambiar Nota");
            System.out.println("6-Salir");

            switch (leer.Entero("Introdude una opcion")) {

                case 1:
                    lis_alu.add(MetodosAlumno.grabarAlumno());
                    break;
                case 2:
                    MetodosAlumno.listarAlumnos(lis_alu);
                    break;
                case 3:
                    MetodosAlumno.buscarAlumnos(lis_alu);
                    break;
                case 4:
                    MetodosAlumno.borrarAlumno(lis_alu);
                    break;
                case 5:
                    MetodosAlumno.cambiarNota(lis_alu);
                    break;

                case 6:
                    salir = true;
                    break;

                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (salir == false);
        MetodosAlumno.guardarEnFichero(fich, lis_alu);

    }

}
