/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer6;

import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author dgf
 */
public class MetodosAlumno {

    static Alumno grabarAlumno()  {
        Leer leer = new Leer();
        String nombre;
        int calificacion;

        nombre = leer.Cadena("Introduce nombre");
        calificacion = leer.Entero("Introduce sueldo");

        Alumno alumno = new Alumno(nombre, calificacion);

        return alumno;

    }

    static void listarAlumnos(ArrayList<Alumno> lista) {

        if (lista.size() >= 1) {
            for (int i = 0; i < lista.size(); i++) {
                System.out.println(lista.get(i));
            }
        } else {
            System.out.println("No hay alumnos en la lista");
        }

    }

    static void borrarFichero(File fich) {
        if (fich.exists()) {
            fich.delete();

        }

    }

    static void renombrarFichero(File fich) {
        Leer leer = new Leer();
        String nombre = leer.Cadena("Que nombre desea poner al fichero");

        if (fich.exists()) {
            File fich2 = new File(nombre + ".txt");
            fich.renameTo(fich2);
        } else {
            System.out.println("no existe");
        }

    }

    static void buscarAlumnos(ArrayList<Alumno> lista) {
        String nombre;
        boolean encontrado = false;

        Leer leer = new Leer();

        nombre = leer.Cadena("Introduce nombre  buscar");

        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getNombre().equalsIgnoreCase(nombre)) {
                System.out.println(lista.get(i).toString());
                encontrado = true;
            }
        }
        if (encontrado == false) {
            System.out.println("No existe el alumno");
        }
    }

    static void borrarAlumno(ArrayList<Alumno> lista) {
        String nombre;
        boolean encontrado = false;

        Leer leer = new Leer();

        nombre = leer.Cadena("Introduce nombre  a borrar");

        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getNombre().equalsIgnoreCase(nombre)) {
                lista.remove(i);
                encontrado = true;
            }
        }
        if (encontrado == false) {
            System.out.println("No existe el alumno");
        }

    }

    static void cambiarNota(ArrayList<Alumno> lista) {
        String nombre;
        int nota;
        boolean encontrado = false;

        Leer leer = new Leer();

        nombre = leer.Cadena("Introduce nombre alumno que desea cambiar su calificacion");
        nota=leer.Entero("Introduce nueva nota");

        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getNombre().equalsIgnoreCase(nombre)) {
                lista.get(i).setCalificacion(nota);
                encontrado = true;
            }
        }
        if (encontrado == false) {
            System.out.println("No existe el alumno");
        }
    }

    static ArrayList<Alumno> leeDerFichero(File fich) throws FileNotFoundException, IOException, ClassNotFoundException {
        ArrayList<Alumno> lista = new ArrayList();
        FileInputStream fichLect = new FileInputStream(fich);
        ObjectInputStream fluent = new ObjectInputStream(fichLect);
        lista = (ArrayList) fluent.readObject();
        fluent.close();
        fichLect.close();
        return lista;

    }

    static void guardarEnFichero(File fich, ArrayList alumnos) throws FileNotFoundException, IOException {
        borrarFichero(fich);
        FileOutputStream fichEsc = new FileOutputStream(fich);
        ObjectOutputStream flusal = new ObjectOutputStream(fichEsc);
        flusal.writeObject(alumnos);
        flusal.close();
        fichEsc.close();
    }

}
