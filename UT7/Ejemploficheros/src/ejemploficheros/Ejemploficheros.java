/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author dgf
 */
public class Ejemploficheros {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) throws FileNotFoundException, IOException {
        String cadena;
        File fichero = new File("ficheros/fichero.txt");
        if (fichero.exists()) {
            BufferedReader flujo = new BufferedReader(new FileReader(fichero));
            System.out.print("\n\nEsta es la informacion que contiene el fichero: \n");
            cadena = flujo.readLine();
            System.out.println(cadena);
        } else {
            System.out.println("El fichero no existe");
        }
        System.out.println("\n¡FIN DEL PROGRAMA!");
        
        
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\n\n\tIddique el nombre de un fichero: ");
        	  String nombre=teclado.readLine();
          	fichero= new File("ficheros/"+nombre);
          	if (fichero.exists())
             		 System.out.println("\t\tOK.Este fichero existe");
         	 else
             		 System.out.println("\t\tError.Este fichero no existe");

    }

    
}
