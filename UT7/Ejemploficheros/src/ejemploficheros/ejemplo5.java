/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploficheros;

import java.io.*;

/**
 *
 * @author dgf
 */
public class ejemplo5 {

    /**
     * @param args the command line arguments
     */
  
      public static void main(String[] args) throws IOException {
           Leer leer=new Leer();
        FileWriter fichEsc = new FileWriter("ejemplo5a.txt");
        int leeByte = 0;
        char caracter;
        String cadena;
        
        cadena=leer.Cadena("Que desea escribir en el fichero?");
        fichEsc.write(cadena);
        fichEsc.close();
        
        FileReader fichLect = new FileReader("ejemplo5a.txt");
        
        while (leeByte != -1) {
            caracter = (char) leeByte;
            System.out.print(caracter);
            leeByte = fichLect.read();
        }
        fichLect.close();
    }

    }
    

