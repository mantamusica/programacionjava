/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploficheros;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author dgf
 */
public class ejemplo3 {

    /**
     * @param args the command line arguments
     */

       public static void main(String[] args) throws IOException{
         int c;
         try{
             FileInputStream f=new FileInputStream("/ejemplo.txt");
             
             while((c=f.read())!=-1)
                     System.out.println((char)c);
                     f.close();
                     
         }
         catch (FileNotFoundException e){
             System.out.println("¡Lo siento mucho. No se puede leer el fichero ya que no existe!");
         }
     }

    }
    

