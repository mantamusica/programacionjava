/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploficheros;

import java.io.*;
/**
 *
 * @author dgf
 */
public class ejemplo4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
       Leer leer=new Leer();
        String cadena;
        int leeByte = 0;
        char caracter;
        
        cadena=leer.Cadena("Que desea escribir en el fichero?");
        
        FileOutputStream fichEsc = new FileOutputStream("ejemplo4a.txt", true);
        
        for (int pos = 0; pos < cadena.length(); pos++) {
            fichEsc.write(cadena.charAt(pos));
        }
        
      //Para separar una frase de otra
        fichEsc.close();
        System.out.println("\n\nEl contenido del fichero es:\n");
        FileInputStream fichLec = new FileInputStream("ejemplo4a.txt");
        
        while (leeByte != -1) {
            caracter = (char) leeByte;
            System.out.print(caracter);
            leeByte = fichLec.read();
        }
        fichLec.close();
    }

    
}
