/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hoja2;

import java.io.*;

/**
 *
 * @author dgf
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        Leer leer = new Leer();
        String nombre, salida;
        int sueldo, antiguedad;

        File fich = new File("hoja2.txt");

        do {
            nombre = leer.Cadena("Introduce nombre");
            sueldo = leer.Entero("Introduce sueldo");
            antiguedad = leer.Entero("Introduce antiguedad");

            Empleados empleado = new Empleados(nombre, sueldo, antiguedad);

            

            FileOutputStream fos;
            ObjectOutputStream oos;

            if (fich.exists()) {
                fos = new FileOutputStream(fich, true);
                oos = new serializacion2.MiObjectOutputStream(fos);
            } else {
                fos = new FileOutputStream(fich);
                oos = new ObjectOutputStream(fos);
            }

            oos.writeObject(empleado);
            oos.close();
            
            salida = leer.Cadena("Desea introducir mas empleados? ");
        } while (!salida.equalsIgnoreCase("n"));
        
        
        FileInputStream fis = new FileInputStream(fich);
        ObjectInputStream ois = new ObjectInputStream(fis);
        
        sueldo=leer.Entero("Introduce el sueldo minimo de los empleados que desea mostrar");
        try {
            while (true) {
                Empleados pepe = (Empleados) ois.readObject();
                
                if(pepe.getSueldo()>sueldo){
                System.out.println(pepe); }            
            }
            
        } catch (EOFException eof) {
            System.out.println("Final de fichero");
        }
        
        
    }

}
