/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectoEmpleados;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author dgf
 */
public class MetodosEmpleado {

    static File fich = new File("clientes.dat");

    static void agregarEmpleado() throws IOException, ClassNotFoundException {
        int codigo;
        String nombre, departamento;
        Leer leer = new Leer();

        do {
            codigo = leer.Entero("Introduce codigo");
        } while (MetodosEmpleado.validarExiste(codigo));

        nombre = leer.Cadena("Introduce nombre");

        do {
            departamento = leer.Cadena("Introduce departamento (Ventas ,Administracion o Produccion");
        } while (!MetodosEmpleado.validarDep(departamento));

        Empleados empleado = new Empleados(codigo, nombre, departamento);

        FileOutputStream fos;
        ObjectOutputStream oos;

        if (fich.exists()) {
            fos = new FileOutputStream(fich, true);
            oos = new MiObjectOutputStream(fos);
        } else {
            fos = new FileOutputStream(fich);
            oos = new ObjectOutputStream(fos);
        }

        oos.writeObject(empleado);
        fos.close();
        oos.close();
    }

    static void listarEmpleados() throws IOException, ClassNotFoundException {
        
        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            try {
                while (true) {
                    Empleados emple = (Empleados) ois.readObject();
                    System.out.println(emple);
                }

            } catch (EOFException eof) {
                fis.close();
                ois.close();
            }

        } else {
            System.err.println("No existe fichero de datos ");
        }

    }

    static void buscarDep() throws IOException, ClassNotFoundException {
        String departamento;
        Boolean encontrado = false;

        if (fich.exists()) {
            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            Leer leer = new Leer();
            do {

                departamento = leer.Cadena("Introduce departamento que desea visualizar (Ventas ,Administracion o Produccion");

            } while (!MetodosEmpleado.validarDep(departamento));

            try {
                while (true) {
                    Empleados emple = (Empleados) ois.readObject();

                    if (emple.getDepartamento().equalsIgnoreCase(departamento)) {
                        System.out.println(emple.toString());
                        encontrado = true;
                    }
                }

            } catch (EOFException eof) {
                ois.close();
                fis.close();
            }

            if (encontrado == false) {
                System.out.println("No existe empleado con ese codigo");
            }

        } else {
            System.err.println("No existe fichero de datos");
        }

    }

    static void borrarEmpleado() throws IOException, ClassNotFoundException {
        int codigo;
        Boolean encontrado = false;
        File demo = new File("demo.txt");

        if (fich.exists()) {
            
            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            FileOutputStream fos;
            ObjectOutputStream oos;

            Leer leer = new Leer();
            codigo = leer.Entero("Introduce codigo del empleado a borrar");

            try {
                while (true) {
                    Empleados emple = (Empleados) ois.readObject();

                    if (emple.getCodigo() != codigo) {

                        if (demo.exists()) {
                            fos = new FileOutputStream(demo, true);
                            oos = new MiObjectOutputStream(fos);
                        } else {
                            fos = new FileOutputStream(demo);
                            oos = new ObjectOutputStream(fos);
                        }

                        oos.writeObject(emple);
                        oos.close();
                        fos.close();
                        
                    } else {
                        System.out.println("Empleado borrado");
                        encontrado = true;
                    }
                }

            } catch (EOFException eof) {
                ois.close();
                fis.close();
            }

            if (encontrado == false) {
                System.out.println("No existe empleado con ese codigo");
            }

            fich.delete();
            demo.renameTo(fich);
        } else {
            System.err.println("No existe fichero de datos");
        }
    }

    static void borrarFichero() {
        if (fich.exists()) {
            fich.delete();
            System.out.println("Fichero Borrado");
        } else {
            System.err.println("No existe fichero de datos");
        }
    }

    static boolean validarDep(String dep) {
        boolean validar = false;

        if ((dep.equalsIgnoreCase("ventas")) || (dep.equalsIgnoreCase("administracion")) || (dep.equalsIgnoreCase("produccion"))) {
            validar = true;
        } else {
            System.out.println("Departamento incorrecto");
        }
        return validar;

    }

    static boolean validarExiste(int codigo) throws IOException, ClassNotFoundException {

        boolean validar = false;

        if (fich.exists()) {

            FileInputStream fis = new FileInputStream(fich);
            ObjectInputStream ois = new ObjectInputStream(fis);

            try {
                while (true) {
                    Empleados emple = (Empleados) ois.readObject();

                    if (emple.getCodigo() == codigo) {
                        validar = true;
                    }
                }

            } catch (EOFException eof) {
                fis.close();
                ois.close();
            }
            if (validar == true) {
                System.err.println("Ya existe un empleado con el codigo ");
            }

        }

        return validar;
    }

}
