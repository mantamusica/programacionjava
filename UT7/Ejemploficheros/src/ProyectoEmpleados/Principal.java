/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectoEmpleados;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author dgf
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Leer leer = new Leer();
        
        

        boolean salir = false;
        int op;

        do {
            System.out.println("1	Alta de empleado");
            System.out.println("2	Listar empleados");
            System.out.println("3	Buscar empleados por departamento");
            System.out.println("4	Borrar empleado");
            System.out.println("5	Borrar fichero de empleados completamente");
            System.out.println("6	Salir de la aplicación.");

            switch (leer.Entero("Introdude una opcion")) {

                case 1:
                    MetodosEmpleado.agregarEmpleado();
                    break;
                case 2:
                  MetodosEmpleado.listarEmpleados();
                    break;
                case 3:
                    MetodosEmpleado.buscarDep();
                    break;
                case 4:
                    MetodosEmpleado.borrarEmpleado();
                    break;
                case 5:
                   MetodosEmpleado.borrarFichero();
                    break;
                case 6:
                    salir = true;
                    break;
                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (salir == false);
    }
    
}
