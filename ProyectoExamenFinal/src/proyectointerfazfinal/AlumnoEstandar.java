/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectointerfazfinal;

import proyectoexamenfinal.*;
import java.io.Serializable;

/**
 *
 * @author dgf
 */
public class AlumnoEstandar extends Alumno {
    
   private String curso; 

    public AlumnoEstandar(String curso, Identidad ide, String codigoAlumno) {
        super(ide, codigoAlumno);
        this.curso = curso;
    }

    public AlumnoEstandar() {
    }


    
    



   @Override
    public String mostrarAlumno() {
        return super.mostrarAlumno()+"Curso: "+curso+"Tasas: "+calcularTasas();
    }

    @Override
    public double calcularTasas() {
        int veces=Integer.parseInt(curso.substring(0,1));
        double total=0;
        
        for(int i=0;i<veces;i++){
            total=total+25;
        }
    
        return super.calcularTasas()+total;
    }
    
     public static boolean validarCurso(String codigo) {
        boolean salir = false;

            if (codigo.matches("\\d{1}+\\º")) {
                salir = true;
            }
        
        return salir;
    }


   

   
   
    
}
