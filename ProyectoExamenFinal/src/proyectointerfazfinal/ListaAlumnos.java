/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectointerfazfinal;

import java.io.EOFException;
import proyectoexamenfinal.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author dgf
 */
public class ListaAlumnos {

    private ArrayList<Alumno> alumnos = new ArrayList();
    private File fich = new File("almacen.txt");

    public void visualizarAlumnos(JTextArea caja) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        caja.setText(null);

        if (fich.exists()) {
            try {
                fis = new FileInputStream(fich);;
                ois = new ObjectInputStream(fis);

                while (true) {
                    Alumno alu = (Alumno) ois.readObject();
                    caja.append(alu.mostrarAlumno());
                    caja.append("\n");
                }

            } catch (Exception e) {

            }

            try {
                fis.close();
                ois.close();
            } catch (IOException ioe) {

            }
            //Mensaje de error sino existe el fichero 
        }

    }

    public boolean borrarAlumno(String codigo) {
        Boolean encontrado = false;
        File demo = new File("demo.txt");
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        if (fich.exists()) {
            try {
                fis = new FileInputStream(fich);
                ois = new ObjectInputStream(fis);

                FileOutputStream fos;
                ObjectOutputStream oos;

                while (true) {
                    Alumno alu = (Alumno) ois.readObject();

                    if (!alu.getCodigoAlumno().equalsIgnoreCase(codigo)) {

                        if (demo.exists()) {
                            fos = new FileOutputStream(demo, true);
                            oos = new MiObjectOutputStream(fos);
                        } else {
                            fos = new FileOutputStream(demo);
                            oos = new ObjectOutputStream(fos);
                        }

                        oos.writeObject(alu);
                        oos.close();
                        fos.close();

                    } else {
                        encontrado = true;
                    }
                }

            } catch (Exception e) {
            }

            try {
                fis.close();
                ois.close();
            } catch (IOException ioe) {
            }

            fich.delete();
            demo.renameTo(fich);
        }
        return encontrado;
    }

    public void mostrarIngresos(JTextField salida) {
        double total = 0;

        FileInputStream fis = null;
        ObjectInputStream ois = null;

        if (fich.exists()) {
            try {
                fis = new FileInputStream(fich);;
                ois = new ObjectInputStream(fis);

                while (true) {
                    Alumno alu = (Alumno) ois.readObject();
                    total = alu.calcularTasas() + total;
                }

            } catch (Exception e) {
                salida.setText(String.valueOf(total));
            }

            try {
                fis.close();
                ois.close();
            } catch (IOException ioe) {

            }
            //Mensaje de error sino existe el fichero 
        }

    }

    public boolean contiene(String codigo) {

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        boolean encontrado=false;

        if (fich.exists()) {
            try {
                fis = new FileInputStream(fich);
                ois = new ObjectInputStream(fis);

                while (true) {
                    Alumno alu = (Alumno) ois.readObject();
                    if (alu.getCodigoAlumno().equalsIgnoreCase(codigo)) {
                        encontrado=true;
                    }
                }

            } catch (Exception e) {
            }

            try {
                fis.close();
                ois.close();
            } catch (IOException ioe) {
            }
        }
        return encontrado;
    }
}
