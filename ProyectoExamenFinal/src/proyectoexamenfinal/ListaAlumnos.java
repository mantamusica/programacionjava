/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoexamenfinal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author dgf
 */
public class ListaAlumnos {

    private ArrayList<Alumno> alumnos = new ArrayList();
    private File fich = new File("datos.dat");

    public void visualizarAlumnos() {

        for (Alumno alu : alumnos) {
            alu.mostrarAlumno();
        }
    }
    
    public void añadirAlumno(Alumno alu){
        alumnos.add(alu);
    }

    public void consultarTasas(String codigo) {
        boolean encontrado=false;

        for (Alumno alu : alumnos) {
            
            if (alu.getCodigoAlumno().equals(codigo)) {
                System.out.println("Las tasas del alumno "+codigo+" son "+ alu.calcularTasas());
                encontrado=true;              
            }
        }
        if(!encontrado){
            System.out.println("No existe el alumno");
        }

    }
    
    
    public void borrarAlumno(String codigo){
        boolean encontrado=false;       
        Iterator<Alumno> ite= alumnos.iterator();
        
        while(ite.hasNext()){
            
            if (ite.next().getCodigoAlumno().equalsIgnoreCase(codigo)) {
               ite.remove();
               encontrado=true;
            }
        }
        
        if(!encontrado){
           System.out.println("No existe el alumno"); 
        }
        
        
    }
    
    
    public void mostrarIngresos(){
        double total=0;
        
        for (Alumno alu : alumnos) {
            total=alu.calcularTasas()+total;
        }
        
        System.out.println("Total de ingresos: "+total);
    }

    public ArrayList<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(ArrayList<Alumno> alumnos) {
        this.alumnos = alumnos;
    }
    
    public  void leeFichero() throws FileNotFoundException, IOException, ClassNotFoundException {
        File fich = new File("datos.dat");
        
        if (fich.exists()) {
            FileInputStream fichLect = new FileInputStream(fich);
            ObjectInputStream fluent = new ObjectInputStream(fichLect);
            alumnos = (ArrayList) fluent.readObject();
            fluent.close();
            fichLect.close();
        }

    }

    public void guardarEnFichero() throws FileNotFoundException, IOException {
        File fich = new File("datos.dat");

        FileOutputStream fichEsc = new FileOutputStream(fich);
        ObjectOutputStream flusal = new ObjectOutputStream(fichEsc);
        flusal.writeObject(alumnos);
        flusal.close();
        fichEsc.close();
    }
    
}
