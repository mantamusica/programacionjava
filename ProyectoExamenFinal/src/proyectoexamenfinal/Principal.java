/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoexamenfinal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author dgf
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnsupportedEncodingException, IOException, FileNotFoundException, ClassNotFoundException {
        ListaAlumnos lista = new ListaAlumnos();
        lista.leeFichero();
        Leer leer = new Leer();
        Alumno alu = null;

        boolean salir = false;
        String codigo, nombre, apellidos, curso;
        int opcion = 0;

        do {
            System.out.println("1.	Añadir alumno");
            System.out.println("2.	Visualizar todos los alumnos");
            System.out.println("3.	Consultar tasas");
            System.out.println("4.	Borrar alumno ");
            System.out.println("5.	Mostrar Ingresos");
            System.out.println("6.	Salir");

            switch (leer.Entero("Introduce opcion")) {

                case 1:
                    do {
                        codigo = leer.Cadena("Introduce codigo");
                    } while (!Alumno.validarCodigo(codigo));

                    nombre = leer.Cadena("Introduce nombre");
                    apellidos = leer.Cadena("Introduce apellidos");

                    do {
                        opcion = leer.Entero("Alumno Estandar pulse 0, Alumno Doctorado pulse 1");

                        if (opcion == 0) {
                            do {
                                curso = leer.Cadena("Introduce curso");
                            } while (!AlumnoEstandar.validarCurso(curso));

                            alu = new AlumnoEstandar(curso, new Identidad(nombre, apellidos), codigo);
                            salir = true;
                        }

                        if (opcion == 1) {
                            do {
                                curso = leer.Cadena("Introduce curso");
                            } while (!AlumnoDoctorado.validarNumcurso(curso));

                            alu = new AlumnoDoctorado(curso, new Identidad(nombre, apellidos), codigo);
                            salir = true;

                        }

                    } while (!salir);
                    if (lista.getAlumnos().contains(alu)) {
                        System.out.println("Ya existe el alumno");
                    } else {
                        lista.añadirAlumno(alu);
                    }
                    salir = false;

                    break;
                case 2:
                    lista.visualizarAlumnos();
                    break;
                case 3:
                    codigo = leer.Cadena("Introduce codigo");
                    lista.consultarTasas(codigo);
                    break;
                case 4:
                    codigo = leer.Cadena("Introduce codigo");
                    lista.borrarAlumno(codigo);
                    break;
                case 5:
                    lista.mostrarIngresos();
                    break;

                case 6:
                    salir = true;
                    break;

                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (salir == false);
        lista.guardarEnFichero();

    }

}
