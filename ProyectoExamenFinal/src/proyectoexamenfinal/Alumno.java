/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoexamenfinal;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author dgf
 */
public abstract class Alumno implements Tasable, Serializable {

    private String identidad, codigoAlumno;

    public Alumno(Identidad ide, String codigoAlumno) {
        this.identidad =ide.getNombre()+" "+ide.getApellidos();
        this.codigoAlumno = codigoAlumno;
    }

    public Alumno() {
        
    }

    public String getCodigoAlumno() {
        return codigoAlumno;
    }

    public void setCodigoAlumno(String codigoAlumno) {
        this.codigoAlumno = codigoAlumno;
    }

    @Override
    public double calcularTasas() {
        
        return 300;
    }

    public static boolean validarCodigo(String codigo) {
        boolean salir = false;

            if (codigo.matches("\\d{4}+\\.\\D{1,3}")) {
                salir = true;
            }
        
        return salir;
    }

    public void  mostrarAlumno() {
        System.out.println("Alumno: " + "identidad=" + identidad+ ", codigoAlumno=" + codigoAlumno); 
    }
    
      @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.codigoAlumno);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
      
        return codigoAlumno.equalsIgnoreCase(((Alumno)obj).codigoAlumno);
    }
}
