/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoexamenfinal;

import java.io.Serializable;

/**
 *
 * @author dgf
 */
public class AlumnoDoctorado extends Alumno {

    private String numCurso;

    public AlumnoDoctorado(String numCurso, Identidad ide, String codigoAlumno) {
        super(ide, codigoAlumno);
        this.numCurso = numCurso;
    }

    public AlumnoDoctorado() {
    }

    
 

    @Override
    public void mostrarAlumno() {
        super.mostrarAlumno();
        System.out.println("Curso: "+numCurso);
         System.out.println("Tasas: "+calcularTasas());
    }

    @Override
    public double calcularTasas() {
      int veces=Integer.parseInt(numCurso.substring(0,1));
        double total=0;
        
        for(int i=0;i<veces;i++){
            total=total+150;
        }
    
        return super.calcularTasas()+total;
    }

    public static boolean validarNumcurso(String codigo) {
        boolean salir = false;

        if (codigo.matches("\\d{1,6}")) {
            salir = true;
        }
        return salir;
    }

}
